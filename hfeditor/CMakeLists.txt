set(hfeditor_src_dir "${PROJECT_SOURCE_DIR}/hfeditor/src")
set(hfeditor_inc_dir "${PROJECT_SOURCE_DIR}/hfeditor/include/hfeditor")

set(editor_sources
	${hfeditor_src_dir}/main.cpp
	${hfeditor_src_dir}/wxEditor.cpp
	${hfeditor_src_dir}/MainWindow.cpp
	${hfeditor_src_dir}/SDLSurfacePanel.cpp
	${hfeditor_src_dir}/SizeReportCtrl.cpp
	${hfeditor_src_dir}/SettingsPanel.cpp
	${hfeditor_src_dir}/AssetTree.cpp
	${hfeditor_src_dir}/CodeTextCtrl.cpp
	${hfeditor_src_dir}/CodeTextCtrlPrefs.cpp
)
source_group("Source Files" FILES ${editor_sources})

set(editor_headers
	${hfeditor_inc_dir}/wxEditor.h
	${hfeditor_inc_dir}/MainWindow.h
	${hfeditor_inc_dir}/SDLSurfacePanel.h
	${hfeditor_inc_dir}/SizeReportCtrl.h
	${hfeditor_inc_dir}/SettingsPanel.h
	${hfeditor_inc_dir}/AssetTree.h
	${hfeditor_inc_dir}/CodeTextCtrl.h
	${hfeditor_inc_dir}/CodeTextCtrlPrefs.h
)
source_group("Header Files" FILES ${editor_headers})

#add_definitions(${wxWidgets_DEFINITIONS}) 
if(UNIX)
	add_definitions("`wx-config --cflags`")
endif(UNIX)

include_directories(${hfeditor_inc_dir})

add_executable(hfeditor  ${editor_sources} ${editor_headers})
target_link_libraries(hfeditor game ${wxWidgets_LIBRARIES} ${SDL2_LIBRARY} ${SDLMAIN2_LIBRARY})