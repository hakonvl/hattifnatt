 #include "MainWindow.h"
#include <wx/artprov.h>
#include <wx/clipbrd.h>
#include <wx/spinctrl.h>

#include <wxEditor.h>
#include <messages/GetAssets.h>
#include <messages/GetAsset.h>
#include <hf/assets/Image.h>
MainWindow::MainWindow(wxWindow* parent,
				 wxWindowID id,
				 hf::AppManager* appManager,
				 const wxPoint& pos,
				 const wxSize& size,
				 long style)
: wxFrame(parent, id, "Hattifnatt Editor", pos, size, style),
  m_appManager(appManager)
{
	// tell wxAuiManager to manage this frame
	m_mgr.SetManagedWindow(this);
	
	// set frame icon
	//SetIcon(wxIcon(sample_xpm));
	
	// set up default notebook style
	m_notebook_style = wxAUI_NB_DEFAULT_STYLE | wxAUI_NB_TAB_EXTERNAL_MOVE | wxNO_BORDER;
	m_notebook_theme = 1;
	
	// create menu
	wxMenuBar* mb = new wxMenuBar;
	
	wxMenu* file_menu = new wxMenu;
	file_menu->Append(wxID_EXIT);
	
	wxMenu* view_menu = new wxMenu;
	view_menu->Append(ID_SDLSurfacePanel, _("Create SDL Surface"));
	view_menu->Append(ID_AssetTree, _("Create asset tree"));
	
	wxMenu* options_menu = new wxMenu;

	
	wxMenu* notebook_menu = new wxMenu;

	m_perspectives_menu = new wxMenu;
	m_perspectives_menu->Append(ID_CreatePerspective, _("Create Perspective"));
	m_perspectives_menu->Append(ID_CopyPerspectiveCode, _("Copy Perspective Data To Clipboard"));
	m_perspectives_menu->AppendSeparator();
	m_perspectives_menu->Append(ID_FirstPerspective+0, _("Default Startup"));
	m_perspectives_menu->Append(ID_FirstPerspective+1, _("All Panes"));
	
	wxMenu* help_menu = new wxMenu;
	help_menu->Append(wxID_ABOUT);
	
	mb->Append(file_menu, _("&File"));
	mb->Append(view_menu, _("&View"));
	mb->Append(m_perspectives_menu, _("&Perspectives"));
	mb->Append(options_menu, _("&Options"));
	mb->Append(notebook_menu, _("&Notebook"));
	mb->Append(help_menu, _("&Help"));
	
	SetMenuBar(mb);
	
	CreateStatusBar();
	GetStatusBar()->SetStatusText(_("Ready"));
	
	
	// min size for the frame itself isn't completely done.
	// see the end up wxAuiManager::Update() for the test
	// code. For now, just hard code a frame minimum size
	SetMinSize(wxSize(400,300));
	
	
	
	// prepare a few custom overflow elements for the toolbars' overflow buttons
	
	wxAuiToolBarItemArray prepend_items;
	wxAuiToolBarItemArray append_items;
	wxAuiToolBarItem item;
	item.SetKind(wxITEM_SEPARATOR);
	append_items.Add(item);
	item.SetKind(wxITEM_NORMAL);
	item.SetId(ID_CustomizeToolbar);
	item.SetLabel(_("Customize..."));
	append_items.Add(item);
	
	
	// create some toolbars
	wxAuiToolBar* tb1 = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
										 wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_OVERFLOW);
	tb1->SetToolBitmapSize(wxSize(48,48));

	tb1->Realize();
	
	
	wxAuiToolBar* tb2 = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
										 wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_OVERFLOW | wxAUI_TB_HORIZONTAL);
	tb2->SetToolBitmapSize(wxSize(16,16));
	
	wxBitmap tb2_bmp1 = wxArtProvider::GetBitmap(wxART_QUESTION, wxART_OTHER, wxSize(16,16));
	tb2->SetCustomOverflowItems(prepend_items, append_items);
	tb2->EnableTool(ID_SampleItem+6, false);
	tb2->Realize();
	
	
	wxAuiToolBar* tb3 = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
										 wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_OVERFLOW);
	tb3->SetToolBitmapSize(wxSize(16,16));
	wxBitmap tb3_bmp1 = wxArtProvider::GetBitmap(wxART_FOLDER, wxART_OTHER, wxSize(16,16));

	tb3->SetCustomOverflowItems(prepend_items, append_items);
	tb3->Realize();
	
	
	wxAuiToolBar* tb4 = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
										 wxAUI_TB_DEFAULT_STYLE |
										 wxAUI_TB_OVERFLOW |
										 wxAUI_TB_TEXT |
																																									wxAUI_TB_HORZ_TEXT);
	tb4->SetToolBitmapSize(wxSize(16,16));
	wxBitmap tb4_bmp1 = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16));
	tb4->Realize();
	
	
	wxAuiToolBar* tb5 = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
										 wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_OVERFLOW | wxAUI_TB_VERTICAL);
	tb5->SetToolBitmapSize(wxSize(48,48));
	tb5->SetCustomOverflowItems(prepend_items, append_items);
	tb5->Realize();
	
	// Give this pane an icon, too, just for testing.
	int iconSize = m_mgr.GetArtProvider()->GetMetric(wxAUI_DOCKART_CAPTION_SIZE);
	
	// Make it even to use 16 pixel icons with default 17 caption height.
	iconSize &= ~1;
	
	// create some center panes

	m_mgr.AddPane(CreateNotebook(), wxAuiPaneInfo().Name(wxT("notebook_content")).
	CenterPane().PaneBorder(false));

	// add the toolbars to the manager
	m_mgr.AddPane(tb1, wxAuiPaneInfo().
	Name(wxT("tb1")).Caption(wxT("Big Toolbar")).
	ToolbarPane().Top());
	
	m_mgr.AddPane(tb2, wxAuiPaneInfo().
	Name(wxT("tb2")).Caption(wxT("Toolbar 2 (Horizontal)")).
	ToolbarPane().Top().Row(1));
	
	m_mgr.AddPane(tb3, wxAuiPaneInfo().
	Name(wxT("tb3")).Caption(wxT("Toolbar 3")).
	ToolbarPane().Top().Row(1).Position(1));
	
	m_mgr.AddPane(tb4, wxAuiPaneInfo().
	Name(wxT("tb4")).Caption(wxT("Sample Bookmark Toolbar")).
	ToolbarPane().Top().Row(2));
	m_mgr.GetPane(wxT("tb4")).Show();
	m_mgr.AddPane(tb5, wxAuiPaneInfo().
	Name(wxT("tb5")).Caption(wxT("Sample Vertical Toolbar")).
	ToolbarPane().Left().
	GripperTop());
	
	m_mgr.AddPane(CreateAssetTree(this), wxAuiPaneInfo()
		.Name(wxT("assetTree"))
		.Caption(wxT("Assets"))
		.Left().Layer(1).Position(1)
		.CloseButton(true)
		.MaximizeButton(true)
		.MinSize(wxSize(190, 100))
		.Show());

	m_mgr.AddPane(new wxButton(this, wxID_ANY, _("Test Button")),
				  wxAuiPaneInfo().Name(wxT("tb6")).
				  ToolbarPane().Top().Row(2).Position(1).
				  LeftDockable(false).RightDockable(false));
	
	// make some default perspectives
	
	wxString perspective_all = m_mgr.SavePerspective();
	
	int i, count;
	wxAuiPaneInfoArray& all_panes = m_mgr.GetAllPanes();
	for (i = 0, count = all_panes.GetCount(); i < count; ++i)
		if (!all_panes.Item(i).IsToolbar())
			all_panes.Item(i).Hide();
	m_mgr.GetPane(wxT("assetTree")).Show();
	m_mgr.GetPane(wxT("notebook_content")).Show();
	wxString perspective_default = m_mgr.SavePerspective();
	
	m_perspectives.Add(perspective_default);
	m_perspectives.Add(perspective_all);
	
	// "commit" all changes made to wxAuiManager
	m_mgr.Update();
}

MainWindow::~MainWindow()
{
	m_mgr.UnInit();
}

wxAuiDockArt* MainWindow::GetDockArt()
{
	return m_mgr.GetArtProvider();
}

void MainWindow::DoUpdate()
{
	m_mgr.Update();
}

void MainWindow::OnEraseBackground(wxEraseEvent& event)
{
	event.Skip();
}

void MainWindow::OnSize(wxSizeEvent& event)
{
	event.Skip();
}

void MainWindow::OnSettings(wxCommandEvent& WXUNUSED(evt))
{
	// show the settings pane, and float it
	wxAuiPaneInfo& floating_pane = m_mgr.GetPane(wxT("settings")).Float().Show();
	
	if (floating_pane.floating_pos == wxDefaultPosition)
		floating_pane.FloatingPosition(GetStartPosition());
	
	m_mgr.Update();
}

void MainWindow::OnCustomizeToolbar(wxCommandEvent& WXUNUSED(evt))
{
	wxMessageBox(_("Customize Toolbar clicked"));
}

void MainWindow::OnGradient(wxCommandEvent& event)
{
	int gradient = 0;
	
	switch (event.GetId())
	{
		case ID_NoGradient:         gradient = wxAUI_GRADIENT_NONE; break;
		case ID_VerticalGradient:   gradient = wxAUI_GRADIENT_VERTICAL; break;
		case ID_HorizontalGradient: gradient = wxAUI_GRADIENT_HORIZONTAL; break;
	}
	
		m_mgr.GetArtProvider()->SetMetric(wxAUI_DOCKART_GRADIENT_TYPE, gradient);
		m_mgr.Update();
}

void MainWindow::OnToolbarResizing(wxCommandEvent& WXUNUSED(evt))
{
	wxAuiPaneInfoArray& all_panes = m_mgr.GetAllPanes();
	const size_t count = all_panes.GetCount();
	for (size_t i = 0; i < count; ++i)
	{
		wxAuiToolBar* toolbar = wxDynamicCast(all_panes[i].window, wxAuiToolBar);
		if (toolbar)
		{
			all_panes[i].Resizable(!all_panes[i].IsResizable());
		}
	}
	
		m_mgr.Update();
}

void MainWindow::OnCreatePerspective(wxCommandEvent& WXUNUSED(event))
{
	wxTextEntryDialog dlg(this, wxT("Enter a name for the new perspective:"),
						  wxT("wxAUI Test"));
	
	dlg.SetValue(wxString::Format(wxT("Perspective %u"), unsigned(m_perspectives.GetCount() + 1)));
	if (dlg.ShowModal() != wxID_OK)
		return;
	
	if (m_perspectives.GetCount() == 0)
	{
		m_perspectives_menu->AppendSeparator();
	}
	
		m_perspectives_menu->Append(ID_FirstPerspective + m_perspectives.GetCount(), dlg.GetValue());
		m_perspectives.Add(m_mgr.SavePerspective());
}

void MainWindow::OnCopyPerspectiveCode(wxCommandEvent& WXUNUSED(evt))
{
	wxString s = m_mgr.SavePerspective();
	
	#if wxUSE_CLIPBOARD
	if (wxTheClipboard->Open())
	{
		wxTheClipboard->SetData(new wxTextDataObject(s));
		wxTheClipboard->Close();
	}
	#endif
}

void MainWindow::OnRestorePerspective(wxCommandEvent& evt)
{
	m_mgr.LoadPerspective(m_perspectives.Item(evt.GetId() - ID_FirstPerspective));
}

void MainWindow::OnNotebookPageClose(wxAuiNotebookEvent& evt)
{

}

void MainWindow::OnNotebookPageClosed(wxAuiNotebookEvent& evt)
{
	wxAuiNotebook* ctrl = (wxAuiNotebook*)evt.GetEventObject();
	
	// selection should always be a valid index
	wxASSERT_MSG( ctrl->GetSelection() < (int)ctrl->GetPageCount(),
				  wxString::Format("Invalid selection %d, only %d pages left",
								   ctrl->GetSelection(),
								   (int)ctrl->GetPageCount()) );
	
	evt.Skip();
}

void MainWindow::OnAllowNotebookDnD(wxAuiNotebookEvent& evt)
{
	// for the purpose of this test application, explicitly
	// allow all noteboko drag and drop events
	evt.Allow();
}

wxPoint MainWindow::GetStartPosition()
{
	static int x = 0;
	x += 20;
	wxPoint pt = ClientToScreen(wxPoint(0,0));
	return wxPoint(pt.x + x, pt.y + x);
}


void MainWindow::OnCreateAssetTree(wxCommandEvent& evt)
{
	m_mgr.AddPane(CreateAssetTree(this), wxAuiPaneInfo().
	Caption(wxT("Assets")).
	Float().FloatingPosition(GetStartPosition()).
	//FloatingSize(300,200).
	CloseButton(true).MaximizeButton(true));
	m_mgr.Update();
}

void MainWindow::OnChangeContentPane(wxCommandEvent& evt)
{
	m_mgr.GetPane(wxT("text_content")).Show(evt.GetId() == ID_TextContent);
	m_mgr.GetPane(wxT("tree_content")).Show(evt.GetId() == ID_TreeContent);
	m_mgr.GetPane(wxT("sizereport_content")).Show(evt.GetId() == ID_SizeReportContent);
	m_mgr.GetPane(wxT("html_content")).Show(evt.GetId() == ID_HTMLContent);
	m_mgr.GetPane(wxT("notebook_content")).Show(evt.GetId() == ID_NotebookContent);
	m_mgr.Update();
}

void MainWindow::OnDropDownToolbarItem(wxAuiToolBarEvent& evt)
{
}


void MainWindow::OnTabAlignment(wxCommandEvent &evt)
{

}

void MainWindow::OnExit(wxCommandEvent& WXUNUSED(event))
{
	Close(true);
}

void MainWindow::OnAbout(wxCommandEvent& WXUNUSED(event))
{
	wxMessageBox(_("2D game engine written using C++, Lua, SDL and wxWidgets. \n(c) Copyright 2012-2013, Håkon Viksmo Lie"), _("About Hattifnatt CodeTextCtrlor"), wxOK, this);
}


SDLSurfacePanel* MainWindow::CreateSDLSurfacePanel(wxWindow* parent, SDL_Surface* surface)
{
	SDLSurfacePanel* panel = new SDLSurfacePanel(this, surface);
	return panel;
}

wxAuiNotebook* MainWindow::CreateNotebook()
{
	// create the notebook off-window to avoid flicker
	wxSize client_size = GetClientSize();
	
	if (m_notebook == nullptr)
	{
		m_notebook = new wxAuiNotebook(this, wxID_ANY,
			wxPoint(client_size.x, client_size.y),
			wxSize(430, 200),
			m_notebook_style);
		m_notebook->Freeze();
	}
	wxBitmap page_bmp = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16));
		
	m_notebook->AddPage(CreateCodeTextCtrl(this), wxT("Test CodeTextCtrlor"));
	m_notebook->Thaw();
	return m_notebook;
}

wxDataViewCtrl* MainWindow::CreateAssetTree(wxWindow* parent)
{	
	if (m_assetTree == nullptr)
	{
		m_assetTree = new wxDataViewCtrl(parent, ID_AssetTree, wxDefaultPosition,
			wxDefaultSize, wxDV_SINGLE | wxDV_NO_HEADER);
	}
	m_assetTreeModel = new AssetTreeModel();
	m_assetTree->AssociateModel(m_assetTreeModel);
	m_assetTreeModel->DecRef();

	hf::GetAssets getAssetMsg{};
	m_appManager->SendEvent(getAssetMsg);

	if (getAssetMsg.GetAssetMap() == nullptr)
		return m_assetTree;
	
	for (hf::AssetMap::const_iterator it = getAssetMsg.GetAssetMap()->begin(); it != getAssetMsg.GetAssetMap()->end(); ++it)
	{
		m_assetTreeModel->AddAsset(it->second);
	}

		wxDataViewTextRenderer* tr = new wxDataViewTextRenderer("string", wxDATAVIEW_CELL_ACTIVATABLE );
	wxDataViewColumn *nameCol =
	new wxDataViewColumn("Name", tr, 0, 200, wxALIGN_LEFT);
	nameCol->SetMinWidth(200); // this column can't be resized to be smaller
	m_assetTree->AppendColumn(nameCol);
	return m_assetTree;
}

CodeTextCtrl* MainWindow::CreateCodeTextCtrl(wxWindow* parent)
{
	CodeTextCtrl* ctrl = new CodeTextCtrl(parent);
	return ctrl;
}

void MainWindow::OnAssetActivated(wxDataViewEvent& evt)
{
	if (!m_assetTreeModel->IsContainer(evt.GetItem()))
	{
		std::cout << m_assetTreeModel->GetName(evt.GetItem()) << " " << m_assetTreeModel->GetSuffix(evt.GetItem()) << std::endl;
		hf::GetAsset getMsg(m_assetTreeModel->GetName(evt.GetItem()).ToStdString());
		m_appManager->SendEvent(getMsg);

		if (getMsg.m_asset == nullptr)
		{
			wxMessageDialog error(this, "Failed to get asset " + m_assetTreeModel->GetName(evt.GetItem()), _("Error opening asset"), wxOK | wxICON_ERROR);
			error.ShowModal();
			return;
		}
		OpenAsset(getMsg.m_asset);
	}
}

bool MainWindow::OpenAsset(hf::Asset* asset)
{
	if (asset->GetType() == "image")
		return OpenImage(dynamic_cast<hf::Image*>(asset));
	else if (asset->GetType() == "script")
		return OpenScript(dynamic_cast<hf::Script*>(asset));

	return false;
}

bool MainWindow::OpenScript(hf::Script* asset)
{
	try
	{
		asset->Prefetch();
		CodeTextCtrl* editor = CreateCodeTextCtrl(this);
		editor->LoadScript(asset);
		m_notebook->AddPage(editor, asset->GetPath(), true);

		return true;
	}
	catch (hf::PrefetchError &e)
	{
		wxMessageDialog error(this, "Failed prefetch " + asset->GetPath(), _("Error prefetching asset"), wxOK | wxICON_ERROR);
		error.ShowModal();
		return false;
	}
}
bool MainWindow::OpenImage(hf::Image* asset)
{
	try
	{
		asset->Prefetch();
		m_notebook->AddPage(CreateSDLSurfacePanel(this, asset->GetSurface()), asset->GetPath(), true);
		return true;
	}
	catch (hf::PrefetchError &e)
	{
		wxMessageDialog error(this, "Failed prefetch " + asset->GetPath(), _("Error prefetching asset"), wxOK | wxICON_ERROR);
		error.ShowModal();
		return false;
	}
	
}
BEGIN_EVENT_TABLE(MainWindow, wxFrame)
EVT_ERASE_BACKGROUND(MainWindow::OnEraseBackground)
EVT_SIZE(MainWindow::OnSize)
EVT_MENU(MainWindow::ID_CreatePerspective, MainWindow::OnCreatePerspective)
EVT_MENU(MainWindow::ID_CopyPerspectiveCode, MainWindow::OnCopyPerspectiveCode)
EVT_MENU(MainWindow::ID_AssetTree, MainWindow::OnCreateAssetTree)

EVT_MENU(wxID_EXIT, MainWindow::OnExit)
EVT_MENU(wxID_ABOUT, MainWindow::OnAbout)

//Asset tree
EVT_DATAVIEW_ITEM_ACTIVATED(MainWindow::ID_AssetTree, MainWindow::OnAssetActivated)

EVT_MENU_RANGE(MainWindow::ID_FirstPerspective, MainWindow::ID_FirstPerspective+1000,
			   MainWindow::OnRestorePerspective)
EVT_AUITOOLBAR_TOOL_DROPDOWN(ID_DropDownToolbarItem, MainWindow::OnDropDownToolbarItem)
EVT_AUINOTEBOOK_ALLOW_DND(wxID_ANY, MainWindow::OnAllowNotebookDnD)
EVT_AUINOTEBOOK_PAGE_CLOSE(wxID_ANY, MainWindow::OnNotebookPageClose)
EVT_AUINOTEBOOK_PAGE_CLOSED(wxID_ANY, MainWindow::OnNotebookPageClosed)
END_EVENT_TABLE()