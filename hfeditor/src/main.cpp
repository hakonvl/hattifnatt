#include "wxEditor.h"
#include <game/GameManager.h>
#include <wx/wx.h>
#include <hf/IProcess.h>
#include <SDL.h>
#include <MainWindow.h>
int main(int argc, char *argv[])
{	

	GameManager manager(argc, argv);
	wxEditor editor;
	manager.EditorInit();
	manager.AddProcess(&editor);

	wxEntryStart(argc, argv);
	wxApp::SetInstance(&editor);
	editor.CallOnInit();
	editor.OnRun();
	return 0;
}
