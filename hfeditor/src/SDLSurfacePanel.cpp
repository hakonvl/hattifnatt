#include "SDLSurfacePanel.h"


inline void SDLSurfacePanel::OnEraseBackground(wxEraseEvent &) { /* do nothing */ }

IMPLEMENT_CLASS(SDLSurfacePanel, wxPanel)

BEGIN_EVENT_TABLE(SDLSurfacePanel, wxPanel)
EVT_PAINT(SDLSurfacePanel::OnPaint)
EVT_ERASE_BACKGROUND(SDLSurfacePanel::OnEraseBackground)
EVT_IDLE(SDLSurfacePanel::OnIdle)
END_EVENT_TABLE()

SDLSurfacePanel::SDLSurfacePanel(wxWindow *parent) : wxPanel(parent, IDP_PANEL)
{
	// ensure the size of the wxPanel
	wxSize size(640, 480);
	//SetSize(wxSize(10000, 10000));
	CreateBackground();
	
	SetMinSize(size);
}
  
SDLSurfacePanel::SDLSurfacePanel(wxWindow *parent, SDL_Surface* surface) : wxPanel(parent, IDP_PANEL), m_surface(surface)
{
	// ensure the size of the wxPanel
	if (SDL_MUSTLOCK(surface)) {
		if (SDL_LockSurface(surface) < 0) {
			return;
		}
	}
	SetSize(wxSize(surface->h, surface->w));
	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface(surface);
	}
	CreateBackground();	
}

SDLSurfacePanel::~SDLSurfacePanel() 
{
	//We don't own the texture, so there is no need to destroy it
}

void SDLSurfacePanel::CreateBackground()
{
	Uint32 rmask, gmask, bmask, amask;

	//HACK: It looks like the SDL_BYTEORDER is wrong, so we are skipping this test. The code was copy-pased from SDL wiki, and that might be wrong to. No thinking were applied in the process of making this code. 
	//#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	//rmask = 0xff000000;
	//gmask = 0x00ff0000;
	//bmask = 0x0000ff00;
	//amask = 0x000000ff;
	//#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
	//#endif*/

	m_bgSurface = SDL_CreateRGBSurface(0, GetSize().GetHeight(), GetSize().GetWidth(), 24, rmask, gmask, bmask, amask);
}

void SDLSurfacePanel::OnPaint(wxPaintEvent &)
{
	// can't draw if the m_surface doesn't exist yet
	if (m_bgSurface == NULL) {
		return;
	}
	// lock the surface if necessary
	if (SDL_MUSTLOCK(m_bgSurface)) {
		if (SDL_LockSurface(m_bgSurface) < 0) {
			return;
		}
	}    
	// create a bitmap from our pixel data
	wxBitmap bmp(wxImage(m_bgSurface->w, m_bgSurface->h, 
	static_cast<unsigned char *>(m_bgSurface->pixels), true));

	// unlock the m_surface
	if (SDL_MUSTLOCK(m_bgSurface)) {
		SDL_UnlockSurface(m_bgSurface);
	}
		    
	// paint the m_surface
	wxBufferedPaintDC dc(this, bmp);
}
 
void SDLSurfacePanel::OnIdle(wxIdleEvent &) 
{
	SDL_BlitSurface(m_surface, NULL, m_bgSurface, NULL);
	Refresh(false);
}
	
void SDLSurfacePanel::SetSurface(SDL_Surface* surface)
{
	m_surface = surface;
	if (SDL_MUSTLOCK(surface)) {
		if (SDL_LockSurface(surface) < 0) {
			return;
		}
	}
	SetSize(wxSize(surface->h, surface->w));
	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface(surface);
	}
}