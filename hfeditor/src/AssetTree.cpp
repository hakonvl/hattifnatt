#include "wx/wxprec.h"


#include "wx/wx.h"
#include "AssetTree.h"


AssetTreeModel::AssetTreeModel()
{
	m_root = new AssetTreeModelNode(NULL, "Assets");

	// setup pop music
	m_scripts = new AssetTreeModelNode(m_root, "Scripts");
	m_root->Append(m_scripts);

	m_images = new AssetTreeModelNode(m_root, "Images");
	m_root->Append(m_images);

	m_entities = new AssetTreeModelNode(m_root, "Entities");
	m_root->Append(m_entities);

	m_maps = new AssetTreeModelNode(m_root, "Maps");
	m_root->Append(m_maps);
}

wxString AssetTreeModel::GetLabel(const wxDataViewItem &item) const
{
	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
	if (!node)      // happens if item.IsOk()==false
		return wxEmptyString;

	return node->GetLabel();
}

wxString AssetTreeModel::GetName(const wxDataViewItem &item) const
{
	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
	if (!node)      // happens if item.IsOk()==false
		return wxEmptyString;

	return node->GetName();
}

wxString AssetTreeModel::GetSuffix(const wxDataViewItem &item) const
{
	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
	if (!node)      // happens if item.IsOk()==false
		return wxEmptyString;

	return node->GetSuffix();
}

void AssetTreeModel::AddAsset(const hf::Asset* asset)
{
	
	if (asset->GetType() == "image")
	{
		AddImage(asset);
	}
	else if (asset->GetType() == "script")
	{
		AddScript(asset);
	}
	else if (asset->GetType() == "map")
	{
		AddMap(asset);
	}
	else if (asset->GetType() == "entity")
	{
		AddEntity(asset);
	}
}

void AssetTreeModel::AddImage(const hf::Asset* image)
{
	AssetTreeModelNode* node = new AssetTreeModelNode(m_images, image);
	m_images->Append(node);

	wxDataViewItem child(static_cast<void*>(node));
	wxDataViewItem parent(static_cast<void*>(m_images));
	ItemAdded(parent, child);
}

void AssetTreeModel::AddScript(const hf::Asset* script)
{
	AssetTreeModelNode* node = new AssetTreeModelNode(m_scripts, script);
	m_scripts->Append(node);

	wxDataViewItem child(static_cast<void*>(node));
	wxDataViewItem parent(static_cast<void*>(m_scripts));
	ItemAdded(parent, child);
}

void AssetTreeModel::AddMap(const hf::Asset* map)
{
	AssetTreeModelNode* node = new AssetTreeModelNode(m_maps, map);
	m_maps->Append(node);

	wxDataViewItem child(static_cast<void*>(node));
	wxDataViewItem parent(static_cast<void*>(m_maps));
	ItemAdded(parent, child);
}

void AssetTreeModel::AddEntity(const hf::Asset* entity)
{
	AssetTreeModelNode* node = new AssetTreeModelNode(m_entities, entity);
	m_entities->Append(node);

	wxDataViewItem child(static_cast<void*>(node));
	wxDataViewItem parent(static_cast<void*>(m_entities));
	ItemAdded(parent, child);
}

// void AssetTreeModel::Delete(const wxDataViewItem &item)
// {
// 	wxASSERT("Not yet implemented, delete-functionality of the asset system is needed first");
// 	
// 	/*AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
// 	if (!node)      // happens if item.IsOk()==false
// 		return;
// 
// 	wxDataViewItem parent(node->GetParent());
// 	if (!parent.IsOk())
// 	{
// 		
// 
// 		// don't make the control completely empty:
// 		wxLogError("Cannot remove the root item!");
// 		return;
// 	}
// 
// 	// is the node one of those we keep stored in special pointers?
// 	if (node == m_pop)
// 		m_pop = NULL;
// 	else if (node == m_classical)
// 		m_classical = NULL;
// 	else if (node == m_ninth)
// 		m_ninth = NULL;
// 
// 	// first remove the node from the parent's array of children;
// 	// NOTE: AssetTreeModelNodePtrArray is only an array of _pointers_
// 	//       thus removing the node from it doesn't result in freeing it
// 	node->GetParent()->GetChildren().Remove(node);
// 
// 	// free the node
// 	delete node;
// 
// 	// notify control
// 	ItemDeleted(parent, item);*/
// }

int AssetTreeModel::Compare(const wxDataViewItem &item1, const wxDataViewItem &item2,
	unsigned int column, bool ascending) const
{
	wxASSERT(item1.IsOk() && item2.IsOk());
	// should never happen

	if (IsContainer(item1) && IsContainer(item2))
	{
		wxVariant value1, value2;
		GetValue(value1, item1, 0);
		GetValue(value2, item2, 0);

		wxString str1 = value1.GetString();
		wxString str2 = value2.GetString();
		int res = str1.Cmp(str2);
		if (res) return res;

		// items must be different
		wxUIntPtr litem1 = (wxUIntPtr)item1.GetID();
		wxUIntPtr litem2 = (wxUIntPtr)item2.GetID();

		return litem1 - litem2;
	}

	return wxDataViewModel::Compare(item1, item2, column, ascending);
}

void AssetTreeModel::GetValue(wxVariant &variant,
	const wxDataViewItem &item, unsigned int col) const
{
	wxASSERT(item.IsOk());

	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
	switch (col)
	{
	case 0:
		variant = node->GetLabel();
		break;
	default:
		wxLogError("AssetTreeModel::GetValue: wrong column %d", col);
	}
}

bool AssetTreeModel::SetValue(const wxVariant &variant,
	const wxDataViewItem &item, unsigned int col)
{
	wxASSERT(item.IsOk());

	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
	switch (col)
	{
	case 0:
		node->SetLabel(variant.GetString());
		return true;

	default:
		wxLogError("MyMusicTreeModel::SetValue: wrong column");
	}
	return false;
}

bool AssetTreeModel::IsEnabled(const wxDataViewItem &item,
	unsigned int col) const
{
	wxASSERT(item.IsOk());
	return true; //TODO: Fix this
}

wxDataViewItem AssetTreeModel::GetParent(const wxDataViewItem &item) const
{
	// the invisible root node has no parent
	if (!item.IsOk())
		return wxDataViewItem(0);

	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();

	// "MyMusic" also has no parent
	if (node == m_root)
		return wxDataViewItem(0);

	return wxDataViewItem((void*)node->GetParent());
}

bool AssetTreeModel::IsContainer(const wxDataViewItem &item) const
{
	// the invisble root node can have children
	// (in our model always "MyMusic")
	if (!item.IsOk())
		return true;

	AssetTreeModelNode *node = (AssetTreeModelNode*)item.GetID();
	return node->IsContainer();
}

unsigned int AssetTreeModel::GetChildren(const wxDataViewItem &parent,
	wxDataViewItemArray &array) const
{
	AssetTreeModelNode *node = (AssetTreeModelNode*)parent.GetID();
	if (!node)
	{
		array.Add(wxDataViewItem((void*)m_root));
		return 1;
	}
	if (node->GetChildCount() == 0)
	{
		return 0;
	}

	unsigned int count = node->GetChildren().GetCount();
	for (unsigned int pos = 0; pos < count; pos++)
	{
		AssetTreeModelNode *child = node->GetChildren().Item(pos);
		array.Add(wxDataViewItem((void*)child));
	}

	return count;
}