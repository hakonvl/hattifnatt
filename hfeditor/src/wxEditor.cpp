#include "wxEditor.h"
#include <SDL.h>
#include <hf/IAppManager.h>
#include <hf/Entity.h>
#include <hf/MessageType.h>
#include <hf/messages/GetAsset.h>
#include <hf/messages/GetAssets.h>
#include <hf/messages/WriteMessage.h>


wxEditor::wxEditor():
	IProcess("wxEditor")
{

}

bool wxEditor::Init()
{
	m_appManager->AddListener(this, hf::MessageType::ENTITY_UPDATE);
	m_appManager->AddListener(this, hf::MessageType::MAP_UPDATE);
	return true;
}

bool wxEditor::OnInit()
{

	m_mainWindow = new MainWindow(NULL,
								  wxID_ANY,
								  m_appManager);
	//m_mainWindow.SetMapSurface(m_mapSurface);
	SetTopWindow(m_mainWindow);
	m_mainWindow->Show();
	return true;
}

void wxEditor::OnIdle(wxIdleEvent&)
{
	m_appManager->Tick();
}

void wxEditor::OnLoop()
{

}

bool wxEditor::OnMessage(hf::Message &message)
{
	switch (message.type)
	{
		case hf::MessageType::MAP_UPDATE:
		{
			ParseMapData(static_cast<hf::MapUpdate&>(message));
			return true;
		}
		case hf::MessageType::ENTITY_UPDATE:
		{
			ParseEntityData(static_cast<hf::EntityUpdate&>(message));
		}
		default:
			return false;
	}
}

void wxEditor::ParseMapData(hf::MapUpdate& data)
{
	for(auto& it : data.images)
	{
		try
		{	
			hf::GetAsset getImage(it.name);
			getImage.m_asset->Prefetch();
			m_appManager->SendEvent(getImage);
			m_renderTargets.emplace_back(it.placement, static_cast<hf::Image*>(getImage.m_asset));
		}
		catch (hf::PrefetchError& e)
		{
			hf::WriteMessage error("Could not prefetch image asset " + it.name + ": " + e.what());
			m_appManager->SendEvent(error);
		}
	}
}

void wxEditor::ParseEntityData(hf::EntityUpdate& data)
{
	for(auto& it : data.entityContainer)
	{
		try
		{
			hf::GetAsset getImage(it.GetImageName());
			m_appManager->SendEvent(getImage);
			getImage.m_asset->Prefetch();
			m_renderTargets.emplace_back(it.GetPosition(), static_cast<hf::Image*>(getImage.m_asset));
		}
		catch (hf::PrefetchError& e)
		{
			hf::WriteMessage error("(1)Could not prefetch image asset " + it.GetImageName() + ": " + e.what());
			m_appManager->SendEvent(error);
		}
	}
}


const hf::AssetMap* wxEditor::GetAssetMap() const
{
	hf::GetAssets msg;
	m_appManager->SendEvent(msg);
	
	return msg.GetAssetMap();
}


BEGIN_EVENT_TABLE(wxEditor, wxApp)
EVT_IDLE(wxEditor::OnIdle)
END_EVENT_TABLE()