#ifndef HFEDITOR_WX_MAIN_WINDOW_H
#define HFEDITOR_WX_MAIN_WINDOW_H
#include <map>
#include <wx/wx.h>
#include <wx/aui/aui.h>
#include <wx/aui/aui.h>
#include <wx/wxhtml.h>
#include <wx/grid.h>
#include <hf/IProcess.h>
#include <IAppManager.h>
#include "SDLSurfacePanel.h"
#include "SizeReportCtrl.h"
#include "AssetTree.h"
#include "CodeTextCtrl.h"
class MainWindow : public wxFrame
{
	enum
	{
		ID_SDLSurfacePanel = wxID_HIGHEST+1,
		ID_AssetTree,
		ID_TextContent,
		ID_TreeContent,
		ID_HTMLContent,
		ID_NotebookContent,
		ID_SizeReportContent,
		ID_CreatePerspective,
		ID_CopyPerspectiveCode,
		ID_AllowFloating,
		ID_AllowActivePane,
		ID_TransparentHint,
		ID_VenetianBlindsHint,
		ID_RectangleHint,
		ID_NoHint,
		ID_HintFade,
		ID_NoVenetianFade,
		ID_TransparentDrag,
		ID_NoGradient,
		ID_VerticalGradient,
		ID_HorizontalGradient,
		ID_LiveUpdate,
		ID_AllowToolbarResizing,
		ID_Settings,
		ID_CustomizeToolbar,
		ID_DropDownToolbarItem,
		ID_NotebookNoCloseButton,
		ID_NotebookCloseButton,
		ID_NotebookCloseButtonAll,
		ID_NotebookCloseButtonActive,
		ID_NotebookAllowTabMove,
		ID_NotebookAllowTabExternalMove,
		ID_NotebookAllowTabSplit,
		ID_NotebookWindowList,
		ID_NotebookScrollButtons,
		ID_NotebookTabFixedWidth,
		ID_NotebookArtGloss,
		ID_NotebookArtSimple,
		ID_NotebookAlignTop,
		ID_NotebookAlignBottom,
		
		ID_SampleItem,
		
		ID_FirstPerspective = ID_CreatePerspective+1000
	};
		
public:
	MainWindow(wxWindow* parent,
			   wxWindowID id,
			   hf::AppManager* appManager,
			   const wxPoint& pos = wxDefaultPosition,
			   const wxSize& size = wxDefaultSize,
			   long style = wxDEFAULT_FRAME_STYLE | wxSUNKEN_BORDER);
	
	~MainWindow();
	
	wxAuiDockArt* GetDockArt();
	void DoUpdate();
	
private:
	wxPoint GetStartPosition();
	SDLSurfacePanel* CreateSDLSurfacePanel(wxWindow* parent, SDL_Surface* surface);
	wxAuiNotebook* CreateNotebook();
	wxDataViewCtrl* CreateAssetTree(wxWindow* parent);
	CodeTextCtrl* CreateCodeTextCtrl(wxWindow* parent);


	bool OpenAsset(hf::Asset* asset);
	bool OpenImage(hf::Image* asset);
	bool OpenScript(hf::Script* asset);
	void OnEraseBackground(wxEraseEvent& evt);
	void OnSize(wxSizeEvent& evt);
	
	void OnCreateAssetTree(wxCommandEvent &evt);
	void OnAssetActivated(wxDataViewEvent &evt);
	void OnAssetSelChanged(wxTreeEvent &evt);
	std::string m_currentAsset;

	void OnCreateText(wxCommandEvent& evt);
	void OnChangeContentPane(wxCommandEvent& evt);
	void OnDropDownToolbarItem(wxAuiToolBarEvent& evt);
	void OnCreatePerspective(wxCommandEvent& evt);
	void OnCopyPerspectiveCode(wxCommandEvent& evt);
	void OnRestorePerspective(wxCommandEvent& evt);
	void OnSettings(wxCommandEvent& evt);
	void OnCustomizeToolbar(wxCommandEvent& evt);
	void OnAllowNotebookDnD(wxAuiNotebookEvent& evt);
	void OnNotebookPageClose(wxAuiNotebookEvent& evt);
	void OnNotebookPageClosed(wxAuiNotebookEvent& evt);
	void OnExit(wxCommandEvent& evt);
	void OnAbout(wxCommandEvent& evt);
	void OnTabAlignment(wxCommandEvent &evt);
	
	void OnGradient(wxCommandEvent& evt);
	void OnToolbarResizing(wxCommandEvent& evt);
	void OnManagerFlag(wxCommandEvent& evt);
	void OnNotebookFlag(wxCommandEvent& evt);
	void OnUpdateUI(wxUpdateUIEvent& evt);
	
	void OnPaneClose(wxAuiManagerEvent& evt);
	
	wxAuiManager m_mgr;
	wxArrayString m_perspectives;
	wxMenu* m_perspectives_menu;
	long m_notebook_style;
	long m_notebook_theme;
	
	wxDataViewCtrl* m_assetTree = nullptr;
	AssetTreeModel* m_assetTreeModel = nullptr;
	wxAuiNotebook* m_notebook = nullptr;
	hf::AppManager* m_appManager = nullptr;
	
	DECLARE_EVENT_TABLE()
};



#endif