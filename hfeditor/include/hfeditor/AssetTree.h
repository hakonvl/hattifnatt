#ifndef HFEDITOR_ASSET_TREE_H
#define HFEDITOR_ASSET_TREE_H
#include <wx/dataview.h>
#include <hf/processes/AssetBank.h>
#include <hf/Asset.h>
class AssetTreeModelNode;
WX_DEFINE_ARRAY_PTR(AssetTreeModelNode*, AssetTreeModelNodePtrArray);

class AssetTreeModelNode
{
public:
	AssetTreeModelNode(AssetTreeModelNode* parent,
		const hf::Asset* asset)
	{
		m_parent = parent;

		m_assetName = asset->GetName();
		m_suffix = asset->GetSuffix();

		m_container = false;
	}

	AssetTreeModelNode(AssetTreeModelNode* parent,
		const wxString &branch)
	{
		m_parent = parent;

		m_assetName = branch;

		m_container = true;
	}

	~AssetTreeModelNode()
	{
		// free all our children nodes
		size_t count = m_children.GetCount();
		for (size_t i = 0; i < count; i++)
		{
			AssetTreeModelNode *child = m_children[i];
			delete child;
		}
	}

	AssetTreeModelNode* GetParent()
	{
		return m_parent;
	}
	AssetTreeModelNodePtrArray& GetChildren()
	{
		return m_children;
	}
	AssetTreeModelNode* GetNthChild(unsigned int n)
	{
		return m_children.Item(n);
	}
	void Insert(AssetTreeModelNode* child, unsigned int n)
	{
		m_children.Insert(child, n);
	}
	void Append(AssetTreeModelNode* child)
	{
		m_children.Add(child);
	}
	unsigned int GetChildCount() const
	{
		return m_children.GetCount();
	}

	wxString GetName() const
	{
		return m_assetName;
	}

	wxString GetSuffix() const
	{
		return m_suffix;
	}

	wxString GetLabel() const
	{
		if (m_suffix != "")
			return m_assetName + "." + m_suffix;
		else
			return m_assetName;
	}
	
	void SetLabel(wxString name)
	{
		printf("AssetTreeModelNode::SetLabel not implemented \n");
	}
	wxString GetAssetName() const
	{
		return m_assetName;
	}

	bool IsContainer() const
	{
		return m_container;
	}
	// TODO/FIXME:
	// the GTK version of wxDVC (in particular wxDataViewCtrlInternal::ItemAdded)
	// needs to know in advance if a node is or _will be_ a container.
	// Thus implementing:
	//   bool IsContainer() const
	//    { return m_children.GetCount()>0; }
	// doesn't work with wxGTK when MyMusicTreeModel::AddToClassical is called
	// AND the classical node was removed (a new node temporary without children
	// would be added to the control)
	
private:
	bool m_container;
	
	wxString m_assetName;
	wxString m_suffix;
	AssetTreeModelNode          *m_parent;
	AssetTreeModelNodePtrArray   m_children;
};


// ----------------------------------------------------------------------------
// MyMusicTreeModel
// ----------------------------------------------------------------------------

/*
Implement this data model
Title               Artist               Year        Judgement
--------------------------------------------------------------------------
1: My Music:
2:  Pop music
3:  You are not alone   Michael Jackson      1995        good
4:  Take a bow          Madonna              1994        good
5:  Classical music
6:  Ninth Symphony      Ludwig v. Beethoven  1824        good
7:  German Requiem      Johannes Brahms      1868        good
*/

class AssetTreeModel : public wxDataViewModel
{
public:
	AssetTreeModel();
	~AssetTreeModel()
	{
		delete m_root;
	}

	// helper method for wxLog

	wxString GetLabel(const wxDataViewItem& item) const;
	wxString GetName(const wxDataViewItem& item) const;
	wxString GetType(const wxDataViewItem& item) const;
	wxString GetSuffix(const wxDataViewItem& item) const;


	void AddAsset(const hf::Asset* asset);

	void AddImage(const hf::Asset* asset);

	void AddMap(const hf::Asset* asset);

	void AddScript(const hf::Asset* asset);

	void AddEntity(const hf::Asset* asset);

	//void Delete(const wxDataViewItem &item);

	int Compare(const wxDataViewItem &item1, const wxDataViewItem &item2,
	          	unsigned int column, bool ascending) const;
	// helper methods to change the model


	// implementation of base class virtuals to define model

	virtual unsigned int GetColumnCount() const
	{
		return 1;
	}

	virtual wxString GetColumnType(unsigned int col) const
	{
		return wxT("string");
	}

	virtual void GetValue(wxVariant &variant,
		const wxDataViewItem &item, unsigned int col) const;
	 bool SetValue(const wxVariant &variant,
		const wxDataViewItem &item, unsigned int col);

	virtual bool IsEnabled(const wxDataViewItem &item,
		unsigned int col) const;

	virtual wxDataViewItem GetParent(const wxDataViewItem &item) const;
	virtual bool IsContainer(const wxDataViewItem &item) const;
	virtual unsigned int GetChildren(const wxDataViewItem &parent,
		wxDataViewItemArray &array) const;

private:
	AssetTreeModelNode*   m_root;

	// pointers to some "special" nodes of the tree:
	AssetTreeModelNode*   m_scripts;
	AssetTreeModelNode*   m_images;
	AssetTreeModelNode*   m_entities;
	AssetTreeModelNode*   m_maps;
};
#endif