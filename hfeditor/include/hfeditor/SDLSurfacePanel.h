#ifndef WX_SDL2_PANEL_H
#define WX_SDL_PANEL_H
#include <iostream>

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
 
#include <wx/dcbuffer.h>
#include <wx/image.h>
#include <SDL.h>
 
enum 
{
IDF_FRAME = wxID_HIGHEST + 1,
IDP_PANEL
};
  
/**
 * @class SDLSurfacePanel
 * 
 * @brief Show a SDL2 texture inside a wxPanel
 * @todo Investigate drawing issues when the total area drawn is bigger than the surface
 */

class SDLSurfacePanel : public wxPanel 
{ 
public:
	/**
	 * @brief Creates a new SDLPanel.
	 *
	 * @param parent The wxWindow parent.
	 */
	SDLSurfacePanel(wxWindow *parent);

	/**
	* @brief Creates a new SDLPanel.
	*
	* @param parent The wxWindow parent.
	* @param surface Pointer to the surface to paint
	*/
	SDLSurfacePanel(wxWindow *parent, SDL_Surface* surface);
  
	/**
	 * @brief Destructor for the SDLPanel.
	 */
	~SDLSurfacePanel();

	/**
	* @brief Set pointer to the SDL_Surface to render
	*/
	void SetSurface(SDL_Surface* texture);
private:
	SDL_Surface* m_surface = nullptr; //< The surface to draw
	SDL_Surface* m_bgSurface = nullptr; //< The background surface, which m_surface will be blitted to


	/**
	 * @brief Creates the background surface
	 * @todo Consider how the color of the background should be set. This depends a bit on how this panel will be used, which still is kind of unnown
	 */
	void CreateBackground();
	
	/**
	 * @brief Called to paint the panel.
	 */
	void OnPaint(wxPaintEvent &);
	
	/**
	 * @brief Called to erase the background.
	 */
	void OnEraseBackground(wxEraseEvent &);
	
	/**
	 * @brief Called to update the panel in idle time.
	 */
	void OnIdle(wxIdleEvent &);

	DECLARE_CLASS(SDLSurfacePanel)
	DECLARE_EVENT_TABLE()  
	
};
#endif