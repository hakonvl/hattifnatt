#ifndef HFEDITOR_WXEDITOR_H
#define HFEDITOR_WXEDITOR_H
#include <hf/IProcess.h>
#include <hf/RenderTarget.h>
#include <hf/messages/MapUpdate.h>
#include <hf/messages/EntityUpdate.h>
#include <hf/processes/AssetBank.h>
#include <wx/wx.h>
#include <vector>
#include "MainWindow.h"

#define __WXDEBUG__ 

class MainWindow;
/**
 * @class wxEditor
 * 
 * @brief CodeTextCtrlor GUI writen in wxWidgets 3
 * 
 */
class wxEditor : public wxApp, public hf::IProcess
{
public:
	wxEditor();
	
	/**
	 * @brief Implements iProcess::Init();
	 */
	bool Init();
	
	/**
	 * @brief Implements iProcess:OnLoop()
	 */
	void OnLoop();
	
	/**
	 * @brief Implements IProcess:OnMessage()
	 */
	bool OnMessage(hf::Message& message);
	
	/**
	 * @brief Parse map data
	 */
	void ParseMapData(hf::MapUpdate& data);
	
	/**
	 * @brief Parse entity data
	 */
	void ParseEntityData(hf::EntityUpdate& data);
	
	/**
	 * @brief asks AssetBank for a map over all assets
	 */
	const hf::AssetMap* GetAssetMap() const;
	
	/**
	 * @brief Render the main main view using SDL software blitting
	 */
	void Render();
	
private:
	MainWindow* m_mainWindow;
	
	std::vector<hf::RenderTarget> m_renderTargets;
	SDL_Surface* m_mapSurface = nullptr;
	
	/**
	 * @brief Implements wxApp::OnInit()
	 */
	virtual bool OnInit();
	
	/**
	 * @brief Implements wxApp::OnIdle()
	 */
	virtual void OnIdle(wxIdleEvent &);
	
	DECLARE_EVENT_TABLE() 
};
#endif
