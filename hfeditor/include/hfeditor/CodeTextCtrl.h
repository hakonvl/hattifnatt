#ifndef _CodeTextCtrl_H_
#define _CodeTextCtrl_H_


#include <hf/assets/Script.h>
#include <hf/assets/Map.h>
#include <hf/assets/XMLDoc.h>
#include "CodeTextCtrlPrefs.h" 


enum { //TODO: Move into the class and create new names
	// menu IDs
	myID_PROPERTIES = wxID_HIGHEST,
	myID_CodeTextCtrl_FIRST,
	myID_INDENTINC = myID_CodeTextCtrl_FIRST,
	myID_INDENTRED,
	myID_FINDNEXT,
	myID_REPLACE,
	myID_REPLACENEXT,
	myID_BRACEMATCH,
	myID_GOTO,
	myID_PAGEACTIVE,
	myID_DISPLAYEOL,
	myID_INDENTGUIDE,
	myID_LINENUMBER,
	myID_LONGLINEON,
	myID_WHITESPACE,
	myID_FOLDTOGGLE,
	myID_OVERTYPE,
	myID_READONLY,
	myID_WRAPMODEON,
	myID_ANNOTATION_ADD,
	myID_ANNOTATION_REMOVE,
	myID_ANNOTATION_CLEAR,
	myID_ANNOTATION_STYLE_HIDDEN,
	myID_ANNOTATION_STYLE_STANDARD,
	myID_ANNOTATION_STYLE_BOXED,
	myID_CHANGECASE,
	myID_CHANGELOWER,
	myID_CHANGEUPPER,
	myID_HILIGHTLANG,
	myID_HILIGHTFIRST,
	myID_HILIGHTLAST = myID_HILIGHTFIRST + 99,
	myID_CONVERTEOL,
	myID_CONVERTCR,
	myID_CONVERTCRLF,
	myID_CONVERTLF,
	myID_USECHARSET,
	myID_CHARSETANSI,
	myID_CHARSETMAC,
	myID_PAGEPREV,
	myID_PAGENEXT,
	myID_SELECTLINE,
	myID_CodeTextCtrl_LAST = myID_SELECTLINE,
	myID_WINDOW_MINIMAL,

	// other ID
	myID_ABOUTTIMER,
	myID_UPDATETIMER,

	// dialog find IDs
	myID_DLG_FIND_TEXT,

	// preferences IDs
	myID_PREFS_LANGUAGE,
	myID_PREFS_STYLETYPE,
	myID_PREFS_KEYWORDS,
};

//============================================================================
// declarations
//===========================================================================
class CodeTextCtrlProperties;


//----------------------------------------------------------------------------
//! CodeTextCtrl
class CodeTextCtrl: public wxStyledTextCtrl {
	friend class CodeTextCtrlProperties;

public:
	//! constructor
	CodeTextCtrl (wxWindow *parent, wxWindowID id = wxID_ANY,
		  const wxPoint &pos = wxDefaultPosition,
		  const wxSize &size = wxDefaultSize,
		  long style =
#ifndef __WXMAC__
		  wxSUNKEN_BORDER|
#endif
		  wxVSCROLL
		 );

	//! destructor
	~CodeTextCtrl ();

	// event handlers
	// common
	void OnSize( wxSizeEvent &event );
	// CodeTextCtrl
	void OnCodeTextCtrlRedo (wxCommandEvent &event);
	void OnCodeTextCtrlUndo (wxCommandEvent &event);
	void OnCodeTextCtrlClear (wxCommandEvent &event);
	void OnCodeTextCtrlCut (wxCommandEvent &event);
	void OnCodeTextCtrlCopy (wxCommandEvent &event);
	void OnCodeTextCtrlPaste (wxCommandEvent &event);
	// find
	void OnFind (wxCommandEvent &event);
	void OnFindNext (wxCommandEvent &event);
	void OnReplace (wxCommandEvent &event);
	void OnReplaceNext (wxCommandEvent &event);
	void OnBraceMatch (wxCommandEvent &event);
	void OnGoto (wxCommandEvent &event);
	void OnCodeTextCtrlIndentInc (wxCommandEvent &event);
	void OnCodeTextCtrlIndentRed (wxCommandEvent &event);
	void OnCodeTextCtrlSelectAll (wxCommandEvent &event);
	void OnCodeTextCtrlSelectLine (wxCommandEvent &event);
	//! view
	void OnHilightLang (wxCommandEvent &event);
	void OnDisplayEOL (wxCommandEvent &event);
	void OnIndentGuide (wxCommandEvent &event);
	void OnLineNumber (wxCommandEvent &event);
	void OnLongLineOn (wxCommandEvent &event);
	void OnWhiteSpace (wxCommandEvent &event);
	void OnFoldToggle (wxCommandEvent &event);
	void OnSetOverType (wxCommandEvent &event);
	void OnSetReadOnly (wxCommandEvent &event);
	void OnWrapmodeOn (wxCommandEvent &event);
	void OnUseCharset (wxCommandEvent &event);
	// annotations
	void OnAnnotationAdd(wxCommandEvent& event);
	void OnAnnotationRemove(wxCommandEvent& event);
	void OnAnnotationClear(wxCommandEvent& event);
	void OnAnnotationStyle(wxCommandEvent& event);
	//! extra
	void OnChangeCase (wxCommandEvent &event);
	void OnConvertEOL (wxCommandEvent &event);
	// stc
	void OnMarginClick (wxStyledTextEvent &event);
	void OnCharAdded  (wxStyledTextEvent &event);
	void OnKey  (wxStyledTextEvent &event);

	void OnKeyDown(wxKeyEvent &event);

	//! language/lexer
	wxString DeterminePrefs (const wxString &filename);
	bool InitializePrefs (const wxString &filename);
	bool UserSettings (const wxString &filename);
	LanguageInfo const* GetLanguageInfo () {return m_language;};

	//! load/save file
	bool LoadFile ();
	bool LoadFile (const wxString &filename);
	bool LoadScript(hf::Script* script);
	bool SaveFile ();
	bool SaveFile (const wxString &filename);
	bool Modified ();
	wxString GetFilename () {return m_filename;};
	void SetFilename (const wxString &filename) {m_filename = filename;};

private:
	// file
	wxString m_filename;

	// lanugage properties
	LanguageInfo const* m_language;

	// margin variables
	int m_LineNrID;
	int m_LineNrMargin;
	int m_FoldingID;
	int m_FoldingMargin;
	int m_DividerID;

	wxDECLARE_EVENT_TABLE();
};

//----------------------------------------------------------------------------
//! CodeTextCtrlProperties
class CodeTextCtrlProperties: public wxDialog {

public:

	//! constructor
	CodeTextCtrlProperties (CodeTextCtrl *CodeTextCtrl, long style = 0);

private:

};

#endif // _CodeTextCtrl_H_
