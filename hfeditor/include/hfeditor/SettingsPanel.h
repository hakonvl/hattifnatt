#ifndef HFEDITOR_SETTINGS_PANEL_H
#define HFEDITOR_SETTINGS_PANEL_H
#include <wx/wx.h>
#include "MainWindow.h"
class SettingsPanel : public wxPanel
{
	enum
	    {
			ID_PaneBorderSize = wxID_HIGHEST+1,
			ID_SashSize,
			ID_CaptionSize,
			ID_BackgroundColor,
			ID_SashColor,
			ID_InactiveCaptionColor,
			ID_InactiveCaptionGradientColor,
			ID_InactiveCaptionTextColor,
			ID_ActiveCaptionColor,
			ID_ActiveCaptionGradientColor,
			ID_ActiveCaptionTextColor,
			ID_BorderColor,
			ID_GripperColor
		};
		
public:
	
	SettingsPanel(wxWindow* parent, MainWindow* frame);
				  
private:
	
	wxBitmap CreateColorBitmap(const wxColour& c);
	void UpdateColors();
		
	void OnPaneBorderSize(wxSpinEvent& event);
			
	void OnSashSize(wxSpinEvent& event);
				
	void OnCaptionSize(wxSpinEvent& event);
					
					
	void OnSetColor(wxCommandEvent& event);					
private:
	
	MainWindow* m_frame; //TODO: CHANGE NAME
	wxSpinCtrl* m_border_size;
	wxSpinCtrl* m_sash_size;
	wxSpinCtrl* m_caption_size;
	wxBitmapButton* m_inactive_caption_text_color;
	wxBitmapButton* m_inactive_caption_gradient_color;
	wxBitmapButton* m_inactive_caption_color;
	wxBitmapButton* m_active_caption_text_color;
	wxBitmapButton* m_active_caption_gradient_color;
	wxBitmapButton* m_active_caption_color;
	wxBitmapButton* m_sash_color;
	wxBitmapButton* m_background_color;
	wxBitmapButton* m_border_color;
	wxBitmapButton* m_gripper_color;
	
	DECLARE_EVENT_TABLE()
};


#endif