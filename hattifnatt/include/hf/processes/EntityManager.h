#ifndef HF_ENTITYMANAGER_H
#define HF_ENTITYMANAGER_H
#include "IProcess.h"
#include "Entity.h"
#include "Controller.h"
#include "Geometry.h"
#include <vector>
#include <map>
namespace hf{
	
/**
 * @class EntityManager
 * 
 * @brief A process for managing all the entities in the game. 
 * 
 * Implements methods to create, remove and alter entities.
 */
class EntityManager : public IProcess
{
public:
	using EntityContainer = std::vector<Entity>; //< Typedef for an container to store entities */
	using SkeletonContainer = std::map<std::string, Entity>;
	using HitBoxes = std::vector<iRect>;
	
	/**
	 * @brief Default constructor as required by IProcess
	 */
	EntityManager();
	
	/**
	 * @brief Implementation of IProcess::OnLoop(); 
	 * 
	 * Updates all entities for each frame
	 * 
	 * @return void
	 * @see IProcess::OnLoop()
	 */
	void OnLoop();
	
	/**
	 * @brief Implementation of IProcess::Init(), which inits the entity system
	 * 
	 * @return void
	 * @see IProcess::OnInit()
	 */
	bool Init();
	
	/**
	 * @brief Implementation of IProcess:Event(const Event &events)
	 * 
	 * @param message Reference to the event
	 * @return bool True of the event was consumed.
	 * @see Event
	 * @see IProcess::OnEvent(const Event &event)
	 */
	bool OnMessage(Message &message);
		
	
	Entity* AddEntity(const std::string& name);

	Entity* AddSkeleton(std::string name);

private:
	EntityContainer m_entities; /*< An container for the entities. @see EntityContainer */
	SkeletonContainer m_skeletons; /*< An container for the entity skeletons. */
	HitBoxes m_hitBoxes; /*< Container for all crash boxes, to be populated by MapUpdate */
	double m_speedFactor; /*< Time between each iteration of the main loop. To be populated by the FPS process*/
};
}

#endif // ENTITYMANAGER_H
