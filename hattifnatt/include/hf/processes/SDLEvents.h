#ifndef HF_SDLEVENTS_H
#define HF_SDLEVENTS_H
#include "IProcess.h"
#include "Keys.h"
#include <SDL.h>
namespace hf
{

class SDLEvents : public IProcess
{
public:
	SDLEvents();
	/**
	* @brief Initialize the system
	*
	* @return True on success
	*/
	bool Init();

	void OnLoop();

	/**
	* @brief Implementation of IProcess::OnMessage
	*
	* Recives messages, and executes the belonging code
	*
	* @param message The recived message
	* @return True if the message is consumed
	*/
	bool OnMessage(Message &message);

	Key SDLKeyToHFKey(SDL_Keycode key);
	
};
}
#endif // SDLEVENTS_H
