#ifndef HF_LUABRIDGE_H
#define HF_LUABRIDGE_H
#include "LuaIncludes.h"
#include "IProcess.h"

namespace hf
{
	//http://gamedevgeek.com/tutorials/calling-c-functions-from-lua/
	//http://usefulgamedev.weebly.com/tolua-example.html

	class LuaBridge : public IProcess
	{
	public:
		LuaBridge();
		bool Init();
		bool OnMessage(Message &message);
	private:
		lua_State *m_L;
	
	};
}
#endif // LUABRIDGE_H
