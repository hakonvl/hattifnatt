#ifndef ASSETBANK_H
#define ASSETBANK_H
#include "IProcess.h"
#include "Asset.h"
#include <map>

namespace hf
{

//TODO: Use unordered_map?
using AssetMap = std::map<std::string, Asset*>; /*< Typedef for the asset map */ 

/**
 * @class AssetBank
 * 
 * @brief System for keeping track of all assets used by the engine. The whole system should support loading-, loading- and validating assets
 * 
 */
class AssetBank : public IProcess
{
public:
	/**
	 * @brief Default constcutor
	 */
	AssetBank();
	
	/**
	 * @brief Add a new asset to the asset map
	 * 
	 * @param asset Pointer to the asset to add
	 * @return void 
	 */
	void AddAsset(Asset* asset);
	
	/**
	 * @brief Initialize the system
	 * 
	 * Mostly a placeholder, as it does nothing usefull
	 * 
	 * @return Always true
	 */
	bool Init();
	
	/**
	 * @brief Placeholder for the IProcess::OnLoop. Does nothing
	 * 
	 * @return void
	 */
	void OnLoop();
	
	/**
	 * @brief Implementation of IProcess::OnMessage
	 * 
	 * Recives messages, and executes the belonging code
	 * 
	 * @param message The recived message
	 * @return True if the message is consumed
	 */
	bool OnMessage(Message &message);
	
	/**
	 * @brief Returns an const iterator wrapping the AssetMap
	 * 
	 * @return The const iterator
	 */

	const AssetMap* GetAssetMap() const;
private:
	AssetMap m_assetMap; /*< The asset map */
};

}

#endif // ASSETBANK_H