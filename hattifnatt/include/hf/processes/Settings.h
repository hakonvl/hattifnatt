#ifndef SETTINGS_H
#define SETTINGS_H
#include <map>
#include <string>
#include "IProcess.h"

//TODO: Add write-support to settings.xml
namespace hf
{

typedef std::map<std::string, std::string> SettingsContainer; /*< Container to store loaded settings */

/**
 * @class Settings
 * 
 * @brief A system for storing and loading settings
 */
class Settings : public IProcess
{
public:
	
	Settings();

	/**
	 * @brief Initializes the system, and loads settings from the disk
	 * 
	 * @return Returns true if the settings xml file is propely parsed
	 */
	bool Init();

	/**
	* @brief If the process are registered as a event listener it should implement this method to recive them
	*
	* @param message The recived message
	* @return The implementation should return true if the event are consumed
	*/
	bool OnMessage(Message &message);

	/**
	 * @brief Set a new value to a settings key
	 * 
	 * @param key The key (Setting) to change
	 * @param value The value to apply
	 * @return void
	 */
	void SetValue(std::string key, std::string value);
	
	/**
	 * @brief Get the value paired up with the key
	 * 
	 * @param key The key to look up
	 * @return The value paired up with the key
	 */
	std::string GetValue(std::string key);
private:
	SettingsContainer m_settings; /*< The settings container storing all keys and values */
	
};
}
#endif // SETTINGS_H
