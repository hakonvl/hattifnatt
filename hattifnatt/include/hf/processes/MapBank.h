#ifndef HF_MAPBANK_H
#define HF_MAPBANK_H
#include "IProcess.h"
#include "Geometry.h"
#include "Asset.h"
#include "assets/Map.h"
#include <map>
#include <string>
namespace hf{
	
using MapContainer = std::map<std::string, Map*>; //< To store lists of map ids */

/**
 * @class MapBank
 * 
 * @brief A process for managing maps and their content 
 * 
 * Implements IProcess
 */
class MapBank : public IProcess
{
public:
	/**
	 * @brief Default constructor as required by IProcess
	 */
	MapBank();
	
	/**
	 * @brief Implementation of IProcess::OnLoop(); 
	 * 
	 * Updates all entities for each frame
	 * 
	 * @return void
	 * @see IProcess::OnLoop()
	 */
	void OnLoop();
	
	/**
	 * @brief Implementation of IProcess::Init(), which inits the entity system
	 * 
	 * @todo What happend when there are no map list, or map list is damaged?
	 * @return void
	 * @see IProcess::OnInit()
	 */
	bool Init();

	bool Load(std::string name);

private:
	void SendMapUpdate();

	Map *m_currentMap;
	MapContainer m_mapList; /*< A list of all maps in the game*/
	
};
}

#endif // ENTITYMANAGER_H
