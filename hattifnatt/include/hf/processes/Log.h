#ifndef LOG_H
#define LOG_H
#include "IProcess.h"
#include <fstream>
namespace hf
{
	enum LOG_FILE{
			LOG_DEFAULT,
			LOG_DEBUG,
	};

/**
 * @class Log
 * @brief A simple log system
 * 
 * Supports two logs, one for debug messages, and one for normal messages
 * Does not implement IProcess as IProcess depends on Log.
*/
class Log : public IProcess
{
public:
	/**
	 * @brief Constructor that sets up the objec	
     */
	Log();
	
	/**
	 * @brief Destructor which closes all file streams
     */
	~Log();

	/**
	* @brief Initialize the system
	*
	* @return Always true
	*/
	bool Init();

	/**
	* @brief Placeholder for the IProcess::OnLoop. Does nothing
	*
	* @return void
	*/
	void OnLoop();

	/**
	* @brief Implementation of IProcess::OnMessage
	*
	* Recives messages, and executes the belonging code
	*
	* @param message The recived message
	* @return True if the message is consumed
	*/
	bool OnMessage(Message &message);

	
	/**
	 * @brief Write a log message to the normal log
     *
	 * The method also appends date and time to the message
     *
     * @param message The message to write
	 * @return void
     */
	void Write(std::string message);
	
	/**
	 * @brief Write a message to the debug logDir
	 * 
     * If the log is not opened,  nothing happens.
	 * It also appends date and time to the message
	 * 
     * @param message The message to write
	 * @return void	
     */
	void WriteDebug(std::string message);
	
	/**
	 * @brief Set the state of the debug log
     * 
     * The method will open and close the log and the logs fstream as needed
	 * 
     * @param state The state to set for the debug log
	 * @return True if the log is closed/opened successfully
     */
	bool SetDebugState(bool state);
private:
	const std::string m_defaultLogFile; /*< File name for the default log file */
	const std::string m_debugLogFile; /*< File name for the debug log file */
	bool m_debugState; /*< State for the debug log*/

	std::ofstream m_defaultLog; /*< File stream for the default log */
	std::ofstream m_debugLog; /*< File stream for the debug log */
	
};
}
#endif // LOG_H
