#ifndef HF_RENDER_H
#define HF_RENDER_H
#include "IProcess.h"
#include "assets/Image.h"
#include "RenderTarget.h"
#include <SDL.h>
#include <vector>

namespace hf{

/**
 * @class Render
 * 
 * @brief Render all graphics on the screen using SDL2 and SDL2_Image 
 * 
 */
class Render : public IProcess
{
public:
	/**
	 * @brief Default constructor as required by IProcess
	 */
	Render(bool useWindow = true);
	
	/**
	 *
	 * @brief Destructor, closes windows, and frees resources
	 *
	 */
	
	~Render();
	
	/**
	 * @brief Implementation of IProcess::OnLoop(); 
	 * 
	 * @return void
	 * @see IProcess::OnLoop()
	 */
	void OnLoop();
	
	/**
	 * @brief Implementation of IProcess::Init(), which inits the rendering system
	 * 
	 * @return void
	 * @see IProcess::OnInit()
	 */
	bool Init();
	
	/**
	 * @brief Implementation of IProcess:Event(const Event &events)
	 * 
	 * @param message Reference to the event
	 * @return bool True of the event was consumed.
	 * @see Event
	 * @see IProcess::OnEvent(const Event &event)
	 */
	bool OnMessage(Message &message);

	/**
	 * @brief Returns a pointer to the texture everything is rendered to, if not using a window. Should be used with care!
	 */
	SDL_Texture* GetTexture();
	
private:
	using Targets = std::vector<RenderTarget>;
	using AssetTargets = std::vector<int>; //Placeholder
	using HitBoxRects = std::vector<SDL_Rect>;

	SDL_Texture* m_texture;
	SDL_Window* m_window;
	SDL_Renderer* m_renderer;
	
	Targets m_staticTargets;
	Targets m_entityTargets;
	int m_height;
	int m_width;
	bool m_renderHitBoxes;
	HitBoxRects m_hitBoxiRects;
	const bool m_useWindow;
};
}

#endif // RENDER_H
