#ifndef HF_FPS_H
#define HF_FPS_H

#include "IProcess.h"
#include <chrono>
namespace hf
{
typedef std::chrono::steady_clock FPSClock; /**< Alias for the clock */
/**
 * @class FPS
 * 
 * @brief A process for calculating and capping fps
 */
class FPS : public IProcess
{
public:
	/**
	 * @brief Constructor, sets pointer to all wanted base systems
	 * 
	 */
	FPS();
	
	/**
	 * The OnLoop() method of this process. Calculates FPS, and if sleeps the rest of the frame if needed
	 * 
	 * @return void
	 */
	void OnLoop();
	
	/**
	 * @brief Initializes the process. Currently is does nothing, as most setup is done by the constructor
	 * 
	 * @return True if the process is initlaized successfilly
	 */
	bool Init();
	
	/**
	 * @brief Set the fps cap state. If true, the process wil cap the fps, and if not it will run at full speed
	 * 
	 * @param state The state to sets
	 * @return void
	 */
	void SetFPSCapState(bool state);
	
	/**
	 * @brief Set the fps cap
	 * 
	 * @param fps Max FPS to run at
	 * @return void
	 */
	void SetFPSCap(int fps);
	
	/** 
	 * @brief Get the fps cap
	 * 
	 * @return The current fps cap
	 */
	int GetFpsCap();
	
	/**
	 * @brief Returns the current fps cap state
	 * 
	 * @return Cap state
	 */
	bool GetFPSCapState();
private:
	bool m_FPSCapState; /*< The cap state */
	int m_FPSCap; /*< If m_FPSCapState is true, we cap the fps to this number */
	int m_numFrames; /*< The number of frames that have passed since the constructor was run */
	float m_fps; /*< Current fps*/
	FPSClock::time_point m_lastRun; /*< A time point representing the last run */

};
}
#endif // FPS_H
