#ifndef HF_PROCESSTEST_H
#define HF_PROCESSTEST_H
#include "IProcess.h"
namespace hf
{
class ProcessTest : public IProcess
{

public:
    ProcessTest();
    void OnLoop();
    bool OnMessage(Message &message);
    bool Init();
};
}
#endif // PROCESSTEST_H
