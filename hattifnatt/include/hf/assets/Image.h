#ifndef IMAGE_H
#define IMAGE_H
#include "Asset.h"
#include "Geometry.h"
#include <string>
#include <SDL.h>
#include <chrono>
#include <stdexcept>
namespace hf

{
	/**
	 * @class RenderError
	 * @brief Exception class for throwing exceptions if an image fail to render
	 */
	class RenderError : public std::runtime_error
	{
	public:
		RenderError(std::string imageName, std::string error) : std::runtime_error("Failed to render " + imageName + ": " + error) {};
	private:
		std::string imageName;/*< The asset name of the image that failed to render*/
		std::string error; /*< Error message, from the renderer */
	};
	
	/**
	* @class RenderError
	* @brief Exception class for throwing exceptions if an image fail to render
	* @todo Move to Asset.h, and use for all assets
	*/
	class PrefetchError : public std::runtime_error
	{
	public:
		PrefetchError(std::string imageName, std::string error) : std::runtime_error("Failed to render " + imageName + ": " + error) {};
	private:
		std::string imageName;/*< The asset name of the image that failed to render*/
		std::string error; /*< Error message, from the renderer */
	};
	
	using ImageClock = std::chrono::steady_clock;

	/**
	 * @class Image
	 *
	 * @brief An asset class for containing images. 
	 * 
	 * Supports color keys and animation. Expects each animation frame to be stacked vertically
	 *
	 */
	class Image : public Asset
	{
	public:
		/**
		 * @brief Constructor without color key. If this constructor is used, it assumes that all colors in the image should be displayed
		 * 
		 * @param name Name and identifier of the image
		 * @param width Default width in pixels of the image. Can is some situations be overriden by map etc
		 * @param height Default heigth in pixels of the image. Can is some situations be overriden by map etc
		 * @param numFrames If the image is animated, this is the number of number of frames (Vertically stacked in the png file) in the animation
		 * @param animationTime This is the total duration of the whole animation in ms
		 * @param md5 md5 string of the file. If used, the asset system will check the md5 sum of the png file to check that it is not altered or replaced
		 */
		Image(const std::string name, int width, int height, int numFrames, double animtationTime, const std::string md5 = "");
		
		/**
		 * @brief Constructor with color key. All colors equal to the color key will be replaced with transparent
		 * 
		 * @param name Name and identifier of the image
		 * @param width Default width in pixels of the image. Can is some situations be overriden by map etc
		 * @param height Default heigth in pixels of the image. Can is some situations be overriden by map etc
		 * @param numFrames If the image is animated, this is the number of number of frames (Verticallys stacked in the png file) in the animation
		 * @param animationTime This is the total duration of the whole animation in ms
		 * @param colorKey The color key to use
		 * @param md5 md5 string of the file. If used, the asset system will check the md5 sum of the png file to check that it is not altered or replaced
		 */
		Image(const std::string name, int width, int height, int numFrames, double animationTime, RGBA colorKey, const std::string md5 = "");
		
		/**
		 * @brief Free the SDL texture.
		 */
		~Image();
		
		/**
		 * @brief Calls the validate method on the asset. Actual loading have do be done by Image::PrefetchTexture() as access to the SDL_Renderer is required
		 *
		 * @return True if successfully validated
		 */
		bool Prefetch();
	
		/**
		 * @brief If not loaded, this functions loads the image file and created a texture
		 * 
		 * Also deals with color keys, and calls Image::Prefetch()
		 * 
		 * @param renderer Pointer to the SDL renderer as required by SDL when creating hardware surfaces
		 * @return True if the image file is successfully loaded, or it allready is loaded
		 */
		bool PrefetchTexture(SDL_Renderer* renderer);
		
		/**
		 * @brief Return a pointer to the SDL texture, or nullptr if it is not created
		 * 
		 * @return Pointer to the texture.
		 */
		SDL_Texture* GetTexture() const;

		/**
		 * @brief Return a pointer to the SDL surface, or nullptr if it is not created
		 * 
		 * @return Pointer to the texture.
		 */
		SDL_Surface* GetSurface() const;
		
		
		/**
		 * @brief Render the texture at the given coordinate, and with the given height an width if given as arguments
		 * 
		 * Keeps tracks of the animation time, and swaps images when needed
		 * 
		 * @param renderer Pointer to the SDL renderer
		 * @param iRect The coordinate, height and width of the image should be rendered to
		 */
		bool Render(SDL_Renderer* renderer, iRect iRect);

		/**
		 * @brief Blit the texture to the given dest surface at the given coordinate, and with the given height an width if given as arguments
		 * 
		 * Keeps tracks of the animation time, and swaps images when needed
		 * 
		 * @param surface Pointer to the SDL destination surface
		 * @param iRect The coordinate, height and width of the image should be rendered to
		 */
		bool Blit(SDL_Surface* destination, iRect iRect);
		
		
		/**
		 * @brief Returns the default width of each image frame
		 * 
		 * @return The width in pixels
		 */
		int GetWidth() const;
		
		/**
		 * @brief Returns the default height of each image frame
		 * 
		 * @®eturn The height in pixels
		 */
		int GetHeight() const;
	private:
		SDL_Texture* m_texture; /*< Pointer to the SDL texture */
		SDL_Surface* m_surface; /*< Pointer to the SDL surface */
		int const m_width; /*< The width of each image this file is containing (For animations) */
		int const m_height; /*< The height of each image this file is containing (For animations) */
		int const m_numFrames; /*< How many frames are this image file containg? In other words: how many frames does this animation contain? */
		float const m_animationTime; /*< How long time should it take to run through the whole animation? */
		ImageClock::time_point m_lastFrameChange; /*< Update each time a frame is changed */
		int m_currentFrame; /*< Which frame is displayed now? Incerment for each Render(). Remember to reset when m_currentFrame > m_num frames */
		RGBA m_colorKey;
		bool m_isPrefetched; /*< To set true when Prefetch() is executed */
	};
}
#endif
