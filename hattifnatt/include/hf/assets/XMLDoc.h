#ifndef XMLDOC_H
#define XMLDOC_H
#include "Asset.h"
#include <string>
namespace hf
{
	/**
	 * @class XMLDoc
	 *
	 * @brief An asset class for holding XML documents
	 * 
	 * @todo Create a better interface, by wrapping rapidxml, for accessing the elements?
	 *
	 */
	class XMLDoc : public Asset
	{
	public:
		/**
		 * @brief Default constructor
		 * 
		 * @param name The name and identifier of the xml document
		 * @param md5 Optional md5 checksum for validating the map
		 */
		XMLDoc(const std::string name, const std::string md5 = "");
		/**
		 * @brief Loads and runs Asset::Validate() the XML file, and then puts the content into m_string
		 *
		 * @return True if the file was successfully loaded and validated
		 */
		bool Prefetch();
		
		/**
		 * @brief Returns the file string
		 * 
		 * @return std::string of the XML file
		 */
		std::string GetFileString() const;
	private:
		std::string m_string; /*<< Raw XML data */
	};
}
#endif
