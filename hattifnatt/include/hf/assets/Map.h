#ifndef MAP_H
#define MAP_H
#include "Asset.h"
#include "Geometry.h"
#include <string>
#include <vector>
namespace hf
{

	/**
	 * @class Map
	 *
	 * @brief An asset class for holding map files, and managing the map properties
	 *
	 */
	class Map : public Asset
	{
	public:

		struct ImageInfo
		{
			ImageInfo(std::string name, iRect placement) : name(name), placement(placement){};
			std::string name;
			iRect placement;
		};
		using ImageInfoContainer = std::vector<ImageInfo>;
		using HitBoxes = std::vector<iRect>;

		/**
		 * @brief Default constructor
		 * 
		 * @param name The name and identifier of the map
		 * @param md5 Optional md5 checksum for validating the map
		 */
		Map(const std::string name, const std::string md5 = "");

		/**
		 * @brief Prefetch the map.Validates and parses the map file
		 *
		 * @return True if the file is successfully parsed and validated
		 */
		bool Prefetch();
		
		/**
		 * @return Const reference to the ImageInforContainer
		 */
		const ImageInfoContainer& GetImageInfoContainer() const;

		/**
		 * @return Const reference to the HitBoxArray
		 */
		const HitBoxes& GetHitBoxes() const;
	private:
		std::string m_xml;/*< Raw XML data */
		ImageInfoContainer m_images; /*< Container for all static images in the map */
		HitBoxes m_hitBoxes; /*< Container for crash boxes */
	};
}
#endif

