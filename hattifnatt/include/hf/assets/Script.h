#ifndef SCRIPT_H
#define SCRIPT_H
#include "Asset.h"
namespace hf
{
	/**
	 * @class Script
	 * 
	 * @brief An asset class for holding scripts
	 * 
	 */
	class Script : public Asset
	{
	public:
		Script(const std::string name, const std::string md5 = "");		
		/**
		 * @brief Prefetch the script.
		 * 
		 * Does not really do anything usefull atm
		 * 
		 * @return Always true
		 */
		bool Prefetch();

		/**
		 * @brief Returns the content of the script
		 */
		std::string Get() const;
	private:
		std::string m_data;
	};
}

#endif // SCRIPT_H
