#ifndef LISTENER_H
#define LISTENER_H
#include "MessageType.h"
#include "IProcess.h"
namespace hf
{
class IProcess;
/**
 * @struct Listener
 * 
 * @brief A struct that contains information about listeners, to be used by EventDispatcher
 * @see IProcess
 */ 
struct Listener
{
	/**
	 * @brief Constructor that sets event type and the pointer to the process
	 * 
	 * @param type The event type the listeer is listening for
	 * @param listener A pointer to the listening process
	 */
	Listener(MessageType type, IProcess* listener) : type(type), listener(listener){};
	const MessageType type; /*< The event type we the listener listens for */
	IProcess* listener; /*< A pointer to the listening process */
};
}
#endif // LISTENER_H
