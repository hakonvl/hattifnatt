#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "IProcess.h"
#include <map>
#include <string>
namespace hf{
	using States = std::map<std::string, bool>; //TODO: Consider using std::aray
	class Controller : public IProcess
	{
	public:
		Controller(std::string name);
		virtual int Evaluate() = 0;
		States GetStates();
	protected:
		States m_states;
	};
}

#endif // CONTROLLER_H
