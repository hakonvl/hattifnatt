#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <ostream>
#include <stdint.h>
#include <cmath>
namespace hf
{

	/**
	 * @file Defines some usefull geometrical shapes and color structs. Typedefs for commonly used version of the templates are found at the bottom
	 */

	/**
	 * @struct RGBA
	 *
	 * Struct that implements a color value in the rgba system. 
	 * Since our origin is in top left corner, all naming are counter clock wise, with A in the top left corner:
	 *
	 *   A---------B
	 *	 |         |
	 *   |         |
	 *   D---------C
	 */
	struct RGBA
	{
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;
	};


	/**
	* @class Vector3
	*
	* 3D vector template, implements common operations
	*/
	template<typename T>
	class Vector3{
	public:
		T x;
		T y;
		T z;

		Vector3() : x{}, y{}, z{} {};
		explicit Vector3(T x, T y, T z) : x{ x }, y{ y }, z{ z }{};

		template<typename NewType>
		Vector3<T> operator +(const Vector3<NewType> &a) const
		{
			return Vector3<T>(x + a.x, y + a.y, z + a.z);
		}

		template<typename NewType>
		Vector3<T> operator -(const Vector3<NewType> &a) const
		{
			Vector3<T> b = Vector3<T>(x - a.x, y - a.y, z - a.z);
			return b;
		}

		template<typename NewType>
		Vector3<T> operator *(const Vector3<NewType> &a) const
		{
			return Vector3<T>(x * a.x, y * a.y, z * a.z);
		}

		template<typename NewType>
		Vector3<T> operator /(const Vector3<NewType> &a) const
		{
			return Vector3<T>(x / a.x, y / a.y, z / a.z);
		}

		template<typename NewType>
		Vector3<T> MultiplyByConst(NewType a) const
		{
			return Vector3<T>(x * a, y * a, z * a);
		}

		template<typename NewType>
		Vector3<T> DivideByConst(NewType a) const
		{
			return Vector3<T>(x / a, y / a, z / a);
		}
		
		template<typename NewType>
		Vector3<T> &operator +=(const Vector3<NewType> &a)
		{
			x += a.x;
			y += a.y;
			z += a.z;

			return *this;
		}

		template<typename NewType>
		Vector3<T> &operator -=(const Vector3<NewType> &a)
		{
			x -= a.x;
			y -= a.y;
			z -= a.z;
			return *this;
		}

		T Length() const
		{
			return static_cast<T>(sqrt(static_cast<double>(x*x) + static_cast<double>(y*y) + static_cast<double>(z*z)));
		}


		bool operator !=(const Vector3<T> &a) const
		{
			return x != a.x || y != a.y || z != a.z;
		}

		template<typename NewType>
		bool operator ==(const Vector3<NewType> &a) const
		{
			return x == a.x && y == a.y && z == a.z;
		}

		
		friend std::ostream& operator<<(std::ostream &o, const Vector3<T> &t)
		{
			o << "(" << t.x << ", " << t.y << ", " << t.z << ")";
			return o;
		}
	};


	/**
	* @class Tetragon
	*
	* Template class for a general tetragone. Can be derived for more spesific tetragons
	* 
	* @todo Take a closer look at the inheritance tree for all geometry classes, and try to find a cleaner soulution
	*/

	template<typename T>
	struct Tetragon
	{
		Vector3<T> a, b, c, d;
		Tetragon() : a{}, b{}, c{}, d{}{};
		explicit Tetragon(Vector3<T> a, Vector3<T> b, Vector3<T> c, Vector3<T> d) : a(a), b(b), c(c), d(d)
		{
		};

		template<typename NewType>
		Tetragon<T> operator +(const NewType &v) const
		{
			return Tetragon<T>(a + v, b + v, c + v, d + v);
		}

		template<typename NewType>
		Tetragon<T> operator -(const NewType &v) const
		{
			return Tetragon<T>(a - v, b - v, c - v, d - v);
		}

		template<typename NewType>
		Tetragon<T> operator +=(const Vector3<NewType> &v)
		{
			a += v;
			b += v;
			c += v;
			d += v;
			return *this;
		}

		template<typename NewType>
		Tetragon<T> operator -=(const Vector3<NewType> &v)
		{
			a -= v;
			b -= v;
			c -= v;
			d -= v;
			return *this;
		}

		T Area() const
		{
			return GetBottom() * GetRight();
		};

		virtual bool Collides(const Tetragon& f) const { return false; };
		Vector3<T> GetA() const
		{
			return a;
		}
		Vector3<T> GetB() const
		{
			return b;
		}
		Vector3<T> GetC() const
		{
			return c;
		}
		Vector3<T> GetD() const
		{
			return d;
		}

		Vector3<T> GetRight() const
		{
			return c - b;
		}

		Vector3<T> GetLeft() const
		{
			return a - d;
		}

		Vector3<T> GetTop() const
		{
			return d - c;
		}

		Vector3<T> GetBottom() const
		{
			return b - a;
		}
	};

	/**
	* @class Rectangle
	*
	* Template class for a iRects
	*/
	template<typename T>
	struct Rectangle : public Tetragon<T>
	{
		Rectangle(){};
		explicit Rectangle(Vector3<T> coordinate, T height, T width) 
		{
			this->a = coordinate; 
			this->b = coordinate;
			this->b.x += width;
			this->d = coordinate;
			this->d.y += height;
			this->c = (this->b - this->a) + (this->d - this->a);
		};
	
		//bool Collides(const iRect& f) const { return false; };
	};

	//Some typedefs
	using iVec3 = Vector3<int>;
	using uiVec3 = Vector3<unsigned int>;
	using fVec3 = Vector3<float>;
	using dVec3 = Vector3<double>;

	using iTetra = Tetragon<int>;
	using uiTetra = Tetragon<unsigned int>;
	using fTetra = Tetragon<float>;
	using dTetra = Tetragon<double>;
	
	using iRect = Rectangle<int>;
	using uiRect = Rectangle<unsigned int>;
	using fRect = Rectangle<float>;
	using dRect = Rectangle<double>;
	


}
#endif
