#ifndef HF_LUAINCLUDES_H
#define HF_LUAINCLUDES_H

/**
 * @file Lua.h
 * 
 * @brief Common file for including lua. Takes care of all required headers and the extern c parameter
 */
extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}
#include <LuaBridge/LuaBridge.h>
#endif
