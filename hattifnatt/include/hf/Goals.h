#ifndef HF_GOALS_H
#define HF_GOALS_H
#include <string>

/*
 //TODO: This class currently serves as a draft, still a lot of work to be done
*/
namespace hf
{
	enum class GoalState
	{
		ACTIVE,
		INACTIVE,
		FAILED,
		SUCCESS
	};
	/**
	 * @class Goal
	 * @brief Basic class for all goal types
	 */
	class BaseGoal
	{
	public:
		BaseGoal(std::string name) : name(name){};
		virtual ~BaseGoal(){};

		virtual double Evaluate() const{};
		
		virtual void SetState(GoalState state) { m_state = state;};
		
		GoalState GetState() const { return m_state; };

		GoalState Process(){ return m_state; };
	private:
		GoalState m_state = GoalState::INACTIVE;
		const std::string m_name;
	};
}

#endif