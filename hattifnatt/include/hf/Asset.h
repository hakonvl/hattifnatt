#ifndef ASSET_H
#define ASSET_H
#include <string>

namespace hf
{

struct Asset
{
public:
	Asset(const std::string name, const std::string type, const std::string suffix, const std::string md5);
	virtual ~Asset();
	virtual bool Prefetch() = 0;
	std::string GetType() const;
	std::string GetSuffix() const;
	std::string GetPath() const;
	std::string GetName() const;
	std::string GetRawData() const;
	bool Validate();
	
private:
	const std::string m_name;
	const std::string m_md5;
	const std::string m_type;
	const std::string m_suffix;

	bool m_loaded;
	bool m_validated;
	
	
};
}
#endif