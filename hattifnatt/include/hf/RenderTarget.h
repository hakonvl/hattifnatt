#ifndef HF_RENDER_TARGET_H
#define HF_RENDER_TARGET_H
#include "Geometry.h"
#include "assets/Image.h"
namespace hf
{
	/**
	 * @struct RenderTarget Struct for paring up an image asset and placement info
	 */
	struct RenderTarget
	{
		RenderTarget(iRect plac, Image* img) : placement(plac), image(img) {}
		iRect placement;
		Image* image;
	};
}

#endif