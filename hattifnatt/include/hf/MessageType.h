#ifndef EVENTTYPE_H
#define EVENTTYPE_H
namespace hf
{
	
	/** 
	 * @enum MessageType 
	 * @brief Enum containing values for all event types
	 */
	enum class MessageType
	{
		TEST,
		ALL,
		FPS_UPDATE,
		ENTITY_UPDATE,
		GET_ASSET,
		GET_ASSETS,
		RUN_SCRIPT,
		GET_SETTING,
		WRITE_MESSAGE,
		GET_LUA_STATE,
		MAP_UPDATE,
		QUIT,
		KEY_STATE_CHANGE
	};
}
#endif
