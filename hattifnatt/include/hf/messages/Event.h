#include "../Message.h"
#include "../MessageType.h"
#include <string>
#include "../Events.h"
#include "../Keys.h"
namespace hf
{
	/** 
	 * @struct Event
	 * 
	 * @brief When the program recieves an event
	 *
	 */
	struct Event : public Message
	{
	public:
		Event(const std::string message, const bool debug = false): Message(MessageType::WRITE_MESSAGE), m_message(message), m_debug(debug) {};
	
		const bool m_debug; /*< True if the message should be written to the debug log */
		const std::string m_message; /*< The message to write */
	};
}
