#ifndef QUIT_H
#define QUIT_H
#include "Message.h"
#include "MessageType.h"

namespace hf
{
	/**
	 * @struct Quit
	 *
	 * @brief Notifies the engine that is is about to quit
	 *
	 */
	struct Quit : public Message
	{
		Quit() : Message(MessageType::QUIT) {};
	};
}
#endif