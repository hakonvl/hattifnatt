#ifndef ENTITY_UPDATE_H
#define ENTITY_UPDATE_H
#include "Message.h"
#include "MessageType.h"
#include "processes/EntityManager.h"

namespace hf
{
/** 
 * @struct EntityUpdate
 * 
 * @brief Message that contains current entity state
 */
struct EntityUpdate : public Message
{
public:
	/**
	 * Constructor that takes current fps value and sets it to m_fps
	 * 
	 * @param entityContainer entityContainer The entity container to send
	 */
	EntityUpdate(const EntityManager::EntityContainer& entityContainer): entityContainer(entityContainer), Message(MessageType::ENTITY_UPDATE) {};

	const EntityManager::EntityContainer& entityContainer;/**< const reference to the entityContainer */
};
}
#endif