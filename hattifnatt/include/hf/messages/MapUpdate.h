#ifndef MAP_UPDATE_H
#define MAP_UPDATE_H
#include "Message.h"
#include "MessageType.h"
#include "assets/Map.h"
#include <string>

namespace hf
{
	struct MapUpdate : public Message
	{
	public:
		MapUpdate(const Map::ImageInfoContainer& a_images, const Map::HitBoxes& a_hitBoxes) : Message(MessageType::MAP_UPDATE), images(a_images), hitBoxes(a_hitBoxes) {};
		const Map::ImageInfoContainer& images;
		const Map::HitBoxes& hitBoxes;

	};
}
#endif