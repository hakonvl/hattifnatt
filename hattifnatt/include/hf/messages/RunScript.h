#ifndef HF_RUN_SCRIPT_H
#define HF_RUN_SCRIPT_H

#include "Message.h"
#include "MessageType.h"
#include "assets/Script.h"
#include <string>

 namespace hf
{
/** 
 * @struct RunScript
 * 
 * @brief Run a lua script
 */
struct RunScript : public Message
{
public:
	/**
	 * @brief Constructor that takes the name of the asset to load, and a pointer to it
	 * 
	 * @param name Name of the asset to load
	 */
	RunScript(const Script& script): Message(MessageType::RUN_SCRIPT), script(script){};

	const Script& script;/*< Name of the asset */
};
}

#endif