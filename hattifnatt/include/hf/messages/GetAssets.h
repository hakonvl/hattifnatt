#include "Message.h"
#include "MessageType.h"
#include "processes/AssetBank.h"
#include <string>

namespace hf
{
	struct GetAssets : public Message
	{
	public:
		GetAssets(std::string type = "") : Message(MessageType::GET_ASSETS), m_type(type) {};
		const AssetMap* GetAssetMap() { return m_assetMap; };
	private:
		friend AssetBank;
		const AssetMap* m_assetMap;
		std::string m_type;
	};
}
