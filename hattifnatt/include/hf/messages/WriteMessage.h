#ifndef WRITE_MESSAGE_H
#define WRITE_MESSAGE_H
#include "Message.h"
#include "MessageType.h"
#include <string>

 namespace hf
{
/** 
 * @struct WriteMessage
 * 
 * @brief Write a message to the log system
 *
 */
struct WriteMessage : public Message
{
public:
	/**
	 * @brief Constructor, taking the message to write and an argument to decide which log we should write to 
	 * 
	 * @param message The message to write
	 * @param debug Set to true if the message should be written to the debug log if available. Default is false
	 */
	WriteMessage(const std::string message, const bool debug = false): Message(MessageType::WRITE_MESSAGE), m_message(message), m_debug(debug) {};
	
	const bool m_debug; /*< True if the message should be written to the debug log */
	const std::string m_message; /*< The message to write */
};
}
#endif