#ifndef HF_GET_LUA_STATE
#define HF_GET_LUA_STATE

#include "Message.h"
#include "MessageType.h"
#include "LuaIncludes.h"
namespace hf
{
/** 
 * @struct GetLuaState
 * 
 * @brief Ask for the lua state pointer
 */
struct GetLuaState : public Message
{
public:
	/**
	 * @brief Constructor that takes the name of the asset to load, and a pointer to it
	*/
	GetLuaState(): Message(MessageType::GET_LUA_STATE){};

	lua_State* m_L = nullptr;
};
}

#endif