#include "Message.h"
#include "MessageType.h"
#include "Asset.h"
#include <string>

namespace hf
{
/** 
 * @struct GetSetting
 * 
 * @brief Ask the settings process for setting
 */
struct GetSetting : public Message
{
public:
	/**
	 * @brief Default constructor, takes the key to look up as the only argument
	 * 
	 * @param key The key to look up
	 */
	GetSetting(const std::string key): Message(MessageType::GET_SETTING), m_key(key) {};
	
	/**
	 * @brief Getter for the key
	 * 
	 * @return The key
	 */
	const std::string GetKey() const { return m_key; };

	/**
	* @brief Getter for the value
	*
	* @return The key
	*/
	std::string GetValue() const { return m_value; };
private:
	const std::string m_key;/*< The key */
	std::string m_value; /*< The value */
	friend class Settings;
};
}
