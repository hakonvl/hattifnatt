#ifndef GET_ASSET_H
#define GET_ASSET_H

#include "Message.h"
#include "MessageType.h"
#include "Asset.h"
#include <string>

 namespace hf
{
/** 
 * @struct GetAsset
 * 
 * @brief Ask for loading a given asset
 */
struct GetAsset : public Message
{
public:
	/**
	 * @brief Constructor that takes the name of the asset to load, and a pointer to it
	 * 
	 * @param name Name of the asset to load
	 * @param asset Pointer to where do put the asset
	 * 
	 */
	GetAsset(std::string name): Message(MessageType::GET_ASSET), m_name(name){};

	const std::string m_name;/*< Name of the asset */
	Asset* m_asset; /*< Pointer to the asset */

};
}

#endif