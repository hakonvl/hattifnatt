#include "Message.h"
#include "MessageType.h"
#include "Keys.h"

namespace hf
{
	/*
	 * @struct KeyStateChange
	 * 
	 * @brief To be sent when a key changes its state
	 *
	 */
	struct KeyStateChange : public Message
	{
	public:
		KeyStateChange(const Key key, bool pressed) : Message(MessageType::KEY_STATE_CHANGE), m_key(key), m_isPressed(pressed) {};
	
		const Key m_key;
		const bool m_isPressed;
	};
}
