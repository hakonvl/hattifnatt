#include "Message.h"
#include "MessageType.h"

 namespace hf
{
/** 
 * @struct FPSUpdate
 * 
 * @brief Message that containt current fps and speed factor
 */
struct FPSUpdate : public Message
{
public:
	/**
	 * @brief Constructor that takes current fps value and sets it to m_fps
	 * 
	 * @param fps The fps value to send
	 */
	FPSUpdate(double fps, double delta): Message(MessageType::FPS_UPDATE), m_fps(fps), m_delta(delta) {};

	double GetFPS() const { return m_fps; };
	double GetDelta() const { return m_delta; };
private: 
	const double m_fps;/**< const float m_fps FPS valut to send */
	const double m_delta; /** Current time between frames. Used from frame rate independent movement */
};
}
