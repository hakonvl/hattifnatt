#ifndef HF_CHECKPATH_H
#define HF_CHECKPATH_H
#include "../Message.h"
#include "../MessageType.h"

namespace hf
{
	/**
	 * @struct CheckPath
	 *
	 * @brief Checks a path against the map
	 *
	 */
	struct CheckPath : public Message
	{
		CheckPath(const std::vector& path) : Message(MessageType::QUIT) {};
		const std::string 
	};
}
#endif