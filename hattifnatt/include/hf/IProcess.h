#ifndef IPROCESS_H
#define IPROCESS_H
#include "Message.h"
#include <string>
#include <vector>
namespace hf
{
class AppManager;

using DependencyContainer = std::vector<std::string>;
/**
 * @class IProcess
 * 
 * @brief A base class for all processes runinng in the engine
 * 
 * Contains virtual functions for all required methods, and a few pointer to important systems
 * Important: Do not rely on the AppManager* m_appManager in the induvidual constructor of the processes. 
 * It is not set until the process is added with AppManager::AddProcess(IProcess* process), right before IProcess::Init() is called
 */
class IProcess
{
public:
	/**
	 * @brief Constructor that sets pointers to all base systems that a process might required
	 * 
	 * @param processName The name of the process
	 */
	IProcess(const std::string processName);
	/**
	 * @brief Destructor
	 */
	virtual ~IProcess();
	/**
	 * @brief A pure virtual functions used to initialize the process
	 * 
	 * The AppManager excepts this to exist when adding new processes
	 * 
	 * @return True if the process initializes successfully
	 */
	virtual bool Init() { return true; };

	/**
	 * @brief The implementation of this function are runned by the main loop on each iteration
	 * 
	 * @todo Does it need to be pure virtual?
	 * @return void
	 */
	virtual void OnLoop() { };
	
	/** 
	 * @brief If the process are registered as a event listener it should implement this method to recive them
	 * 
	 * @param message The recived message
	 * @return The implementation should return true if the event are consumed
	 */
	virtual bool OnMessage(Message &message) { return false; };

	/**
	 * @brief Get the name of the process
	 *
	 * @return std::string of the name
	 */
	const std::string GetName() const;

	/**
	 * @brief Add a dependency
	 *
	 * @return void
	 */
	void AddDependency(std::string procName);

	/**
	 * @return Const reference to the dependency container
	 */
	const DependencyContainer& GetDependencyContainer();

	/**
	* @brief Used to set the pointer to appManager
	*
	* @return void
	*/
	void SetAppManager(AppManager* appManager);
protected:
	const std::string m_name; /*<The name of the process*/
	AppManager* m_appManager; /*<Pointer to the appManager */
	DependencyContainer m_dependencies; /*<Container of the dependencies for this process*/
};
}
#endif // IPROCESS_H
