#ifndef APPMANAGER_H
#define APPMANAGER_H

#include "IProcess.h"
#include "processes/Settings.h"
#include "Listener.h"
#include "Message.h"
#include "MessageType.h"

#include "processes/EntityManager.h"
#include "processes/Log.h"
#include "processes/FPS.h"
#include "processes/AssetBank.h"
#include "processes/MapBank.h"
#include "processes/Render.h"
#include "processes/LuaBridge.h"
#include "processes/SDLEvents.h"

#include <vector>
#include <unordered_map>
#include <string>
//TODO: Change file name

namespace hf
{

using ProcessContainer = std::vector<IProcess*>; /*< Container for processes */
using EventListeners = std::vector<Listener>; /**< Container for holding all listeners */
using ProcessWaitMap = std::unordered_map<std::string, ProcessContainer>; /*< Unordered map to hold processes waiting for their dependencies to be started */

/**
 * @class AppManager
 * 
 * @brief A base class for maintaining a game, editor, server or other app using the engine
 * 
 * Contains basic functionality and systems for the engine, including logging, process support, events
 * and settings management.
 */
class AppManager
{
public:
	/**
	 * @brief Constructor that sets a few variables to we can init the rest of the systems
	 * 
	 * @param argc The number of arguments passed to the application
	 * @param argv The arguments passed to the application
	 */
	AppManager(int argc, char** argv);
	
	/**
	 * @brief Destructor that currently does nothing, but should be used for cleanup
	 */
	virtual ~AppManager();
	
	/**
	 * @brief Inits all systems required for running the game
	 * 
	 * Inits all crucial systems like logging, events, scripting, etc, before we start a few basic processes
	 * It also runs the implementation specific init
	 * 
	 * @see AppManager::OnInit()
	 * @return True if all systems are successfully initialized
	 */
	bool GameInit();

	/**
	* @brief Inits all systems required for running the editor
	*
	* Inits all crucial systems like logging, events, scripting, etc, before we start a few basic processes
	* It also runs the implementation specific init
	*
	* @see AppManager::OnInit()
	* @return True if all systems are successfully initialized
	*/
	bool EditorInit();
	
	/** 
	 * @brief A pure virtual function that inits implementation spesific systems
	 * 
	 * Should be used for adding non-crucial processes etc
	 * 
	 * @returns True if all systems are successfully initialized
	 */
	virtual bool OnInit() = 0;
	
	/**
	 * @brief Enter the main loop
	 *
	 * @return void
	 */ 
	void Loop();

	/**
	 * @brief Run a single iteration of the main loop
	 */
	void Tick();
	
	/**
	* @brief Send and event to all event listeners listening to the event type
	*
	* The function takes a reference, and it does not copy the event. Therefore, the sender have
	* the responsebility to make sure the events exists until the listener have finished using the event.
	*
	* @param event A reference to the message
	* @return void
	*/
	void SendEvent(Message &message);

	/**
	* @brief Add a new listener listening for the given EventType
	*
	* @param listener Pointer to the listener. This is a IProcess pointer, so the listener has to be a processed
	* @param type The MessageType to listen for
	* @return void
	*/
	void AddListener(IProcess* listener, MessageType type);

	/**
	 * Add a new process (implementing IProcess)
	 * 
	 * First is runs the Init() method of the process before pushing it to the process container
	 * 
	 * @param process Pointer to the IProcess to adding
	 * @return True if the Init() method was executed successfully
	 * @see IProcess
	 */ 
	bool AddProcess(IProcess* process);

	/**
	 * @return Returns argc
	 */

	int GetArgc() const;
	/**
	* @return Returns argv
	*/
	char** GetArgv() const;

protected:
	/**
	 * @brief Returns true if the system is successfully initialized
	 * 
	 * @return True if all systems is successfully initialized
	 */
	bool IsInitialized();
	
	/**
	 * @brief Returns true if the system is running, i we are not initializing or shutting down
	 * 
	 * @return Returns true if we are running
	 */
	bool IsRunning();
	
	/**
	 * @brief Get a process by the name
	 *
	 * @param processName std::string of the process name
	 * @return A pointer to the process, or nullptr on failure
	 */
	IProcess* GetProcess(const std::string processName) const;

	/*
	 * @brief Check if a process is runnning
	 *  
	 * TODO: As a temporary solution is used AddProcess and checks the result. Consider using some faster method
	 *
	 * @return True if the process exists
	 */
	bool IsProcessRunning(const std::string processName) const;
	
	//Processes:
	AssetBank m_assetBank;
	Settings m_settings;
	Log m_logProcess;
	FPS m_fps;
	MapBank m_mapBank;
	Render m_render;
	SDLEvents m_events;
	EntityManager m_entityManager;
	LuaBridge m_luaBridge;
	//ProcessTest m_processTest;

private:
	char** m_argv; /*< The arguments passed to the app manager */
	int m_argc; /*< The number of arguments passed to the app manager */
	bool m_isInitialized; /*< True if initialization is successfully finnished */
	bool m_run; /*< True if the main loop are running */
	ProcessContainer m_processes; /*< The process container */
	ProcessWaitMap m_waitingProcesses; /*< Processes waiting to be started, because of missing dependencies */
	EventListeners m_listeners;/**< Container for all event listeners */
	
	
};
}
#endif // IAPPMANAGER_H
