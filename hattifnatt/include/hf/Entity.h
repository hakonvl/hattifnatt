#ifndef ENTITY_H
#define ENTITY_H
#include <string>
#include <vector>
#include "Geometry.h"
#include "Controller.h"
#include <vector>
#include <iostream>
namespace hf{

class EntityManager;
using ControllerContainer = std::vector<Controller*>;
struct Entity
{

public:
    Entity(const std::string name, std::string imageName, iRect position, dVec3 accel) :
        m_name(name),
        m_imageName(imageName),
        m_position(position),
        m_accel(accel),
        m_isGhost(false)
    {};
    Entity(const std::string name):
        m_name(name),
        m_imageName{},
        m_position{},
        m_accel{},
        m_isGhost(false)
    {}
    /**
     * @return Return a string containt the  entity name
     */
    std::string GetName() const { return m_name; };


    /**
     * @return Return a string containing the ImageName of this entity
     */
    std::string GetImageName() const { return m_imageName; };
   
    /**
     * @brief Set image asset name
     */
    void SetImageName(const std::string name){ m_imageName = name; };
    
    /**
     * @brief Set position
     */
    void SetPosistion(iRect pos) { m_position = pos; };
    
    /**
     * @return Return the current size and coordinate, represented by a iRect
     */

    iRect GetPosition() const { return m_position; };
    
    /**
     * @brief Set speed
     */
    void SetSpeed(dVec3 speed) { m_speed = speed; };
    
    /**
     * @return Return current speed
     */
    dVec3 GetSpeed() const { return m_speed; };
    
    /**
     * @brief Set acceleration
     */
    void SetAccel(dVec3 accel) { m_accel = accel; };
    
    /**
     * @return Return current acceleration
     */
    dVec3 GetAccel() const { return m_accel; };
    
    /**
     * @brief Entity derivates should implement this method to do keyboard-, and network input, or AI-thinking
     */
    virtual void Think(double speedFactor) {};


    /**
     * @brief Add a hitbox th the entity
     * @todo Return reference to new iRect
     */
    void AddHitBox(iRect iRect) { m_hitBoxes.emplace_back(iRect); };

    /**
     * @return Returns a vector with all sub crashboxes
     */
    const std::vector<iRect>& GetHitBoxes() const { return m_hitBoxes; };

protected:
    dVec3 m_accel;
    dVec3 m_speed;

    bool m_isGhost; /*< Skip crash checking? */

    const std::string m_name; /**< The name of the entity, in machine-readable format */
    std::vector<iRect> m_hitBoxes; /**< Vector of all sub-hitBoxes, used for finer crash-checking */
    std::string m_imageName; /** Name of the current imageAsset for this entity */
    ControllerContainer m_controllers; /** Container for all Controllers for this entity */
private:
    friend EntityManager;
    iRect m_position; /**< Main hitbox, used for simple and dirty crash-checking, and for positioning in map */


};
}

#endif // ENTITY_H
