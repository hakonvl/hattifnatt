#ifndef MESSAGE_H
#define MESSAGE_H
#include "MessageType.h"
namespace hf
{
	/**
	 * @struct Message
	 * @brief Base class for events.
	 * 
	 * Derive this class to create new events
	 */ 
	struct Message
	{
		/**
		* @brief Constructor that sets event type
		* 
		* @param MessageType The event type for this event
		*/
		Message(const MessageType messageType) : type(messageType){};
	
		const MessageType type; /**< The event type */
	};

}
#endif
