#include <iostream>
#include <game/GameManager.h>
#include <SDL.h>
int main(int argc, char** argv)
{
	GameManager manager(argc, argv);
	
	if(manager.Init())
	{	
		std::cout<<"Init done!"<<std::endl;
	}
	else
	{
		std::cout<<"Init failed!"<<std::endl;
		return 1;
	}
	manager.Loop();
	return 0;
}