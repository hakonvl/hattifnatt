#include <fstream>
#include <sstream>
#include <iostream>
#include "Asset.h"

namespace hf
{
	Asset::Asset(const std::string name, const std::string type, const std::string suffix, const std::string md5) :
		m_name(name),
		m_type(type),
		m_suffix(suffix),
		m_md5(md5),
		m_loaded(false),
		m_validated(false)
	{

	}

	Asset::~Asset()
	{

	}

	bool Asset::Validate()
	{
		m_validated = true;
		return m_validated;
	}

	std::string Asset::GetPath() const
	{
		if (m_validated)
		{
			std::string path = this->GetType();
			path.append("/");
			path.append(m_name);
			path.append(".");
			path.append(this->GetSuffix());
			return path;
		}
		else
		{
			return nullptr;
		}
	}


	std::string Asset::GetName() const
	{
		return m_name;
	}

	std::string Asset::GetSuffix() const
	{
		return m_suffix;
	}

	std::string Asset::GetType() const
	{
		return m_type;
	}



	std::string Asset::GetRawData() const
	{
		std::ifstream file;
		try
		{
			file.open(GetPath());
			if (!file)
			{
				std::string message = "Error opening ";
				message.append(GetPath());
				throw std::ios::failure(message);
			}
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
			return false;
		}
		std::stringstream buffer;

		buffer << file.rdbuf();
		return buffer.str();	
	}
}