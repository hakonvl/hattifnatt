#include "IProcess.h"
#include "IAppManager.h"
namespace hf
{

IProcess::IProcess(const std::string processName):
m_name(processName)
{

}

IProcess::~IProcess()
{

}

const std::string IProcess::GetName() const {
	return m_name;
}

void IProcess::AddDependency(std::string processName)
{
	m_dependencies.push_back(processName);
}

const DependencyContainer& IProcess::GetDependencyContainer()
{
	return m_dependencies;
}

void IProcess::SetAppManager(AppManager* appManager)
{
	m_appManager = appManager;
}
}
