#include "IAppManager.h"
#include "MessageType.h"
#include "messages/WriteMessage.h"
#include <iostream>
#include <unordered_map>

namespace hf
{
	AppManager::AppManager(int argc, char** argv) :
	m_argc(argc),
	m_argv(argv),
	m_isInitialized(false),
	m_run(false)
{
}

AppManager::~AppManager()
{

}

bool AppManager::GameInit()
{
	AddProcess(&m_settings);
	AddProcess(&m_logProcess);
	AddProcess(&m_assetBank);
	AddProcess(&m_events);
	AddProcess(&m_luaBridge);
	AddProcess(&m_entityManager);
	AddProcess(&m_fps);
	AddProcess(&m_render);
	AddProcess(&m_mapBank);
	if(!OnInit())
	{	
		return false;
	}

	m_run = true;
	m_isInitialized = true;

	return true;
}
bool AppManager::EditorInit()
{
	AddProcess(&m_settings);
	AddProcess(&m_logProcess);
	AddProcess(&m_assetBank);
	AddProcess(&m_events);
	AddProcess(&m_luaBridge);
	AddProcess(&m_entityManager);
	//AddProcess(&m_fps);
	//AddProcess(&m_render);
	AddProcess(&m_mapBank);
	if (!OnInit())
	{
		return false;
	}

	m_run = true;
	m_isInitialized = true;

	return true;
}

bool AppManager::IsInitialized()
{
	return m_isInitialized;
}

bool AppManager::IsRunning()
{
	return m_run;
}


void AppManager::Loop()
{
	while (IsRunning())
	{
		Tick();
	}
}

void AppManager::Tick()
{
	for (auto it : m_processes)
	{
		it->OnLoop();
	}
}


bool AppManager::IsProcessRunning(const std::string processName) const
{
	IProcess *process = GetProcess(processName);

	if (process == nullptr)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool AppManager::AddProcess(IProcess* process)
{
	const DependencyContainer& dependencies = process->GetDependencyContainer();

	for (auto& it : dependencies)
	{
		if (!IsProcessRunning(it))
		{
			m_waitingProcesses[it].emplace_back(process);

			std::string messageString = "Added process ";
			messageString.append(process->GetName());
			messageString.append(" to wait map, missing dependency ");
			messageString.append(it);
			WriteMessage message(messageString, true);
			SendEvent(message);
			return false;
		}
	}
	process->SetAppManager(this);
	if(process->Init())
	{
		
		std::string messageString = "Process ";
		messageString.append(process->GetName());
		messageString.append(" was successfully added");
		WriteMessage message = WriteMessage(messageString, true);
		SendEvent(message);
		m_processes.push_back(process);
		//Remember to not return until possible waiting processes are started
	}
	else
	{
		std::string messageString = "Failed to run ";
		messageString.append(process->GetName());
		messageString.append("->Init(), process not added");
		WriteMessage message = WriteMessage(messageString, true);
		SendEvent(message);
		return false;	
	}

	for (auto it : m_waitingProcesses[process->GetName()])
	{
		it->Init();
	}
	
	return true;

}

IProcess* AppManager::GetProcess(const std::string processName) const
{
	for (auto it : m_processes)
	{
		if (it->GetName().compare(processName))
		{
			return it;
		}
	}
	return nullptr;
}

void AppManager::SendEvent(Message &message)
{
	for (auto it : m_listeners)
	{
		if (it.type == message.type)
		{
			it.listener->OnMessage(message);
		}
	}

	if (message.type == MessageType::QUIT)
	{
		m_run = false;
	}
}

void AppManager::AddListener(IProcess* listener, MessageType type)
{
	m_listeners.emplace_back(type, listener);
}

int AppManager::GetArgc() const
{
	return m_argc;
}

char** AppManager::GetArgv() const
{
	return m_argv;
}

}
