#ifndef KBINPUT_H
#define KBINPUT_H
#include "Controller.h"

namespace hf{
	class KBInput : public Controller
	{
	public:
		KBInput();

		bool Init();

		bool OnMessage(Message &message);

		int Evaluate();
		
	};
}

#endif // CONTROLLER_H
