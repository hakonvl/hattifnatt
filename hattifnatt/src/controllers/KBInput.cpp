#include "KBInput.h"
#include "IAppManager.h"
#include "messages/KeyStateChange.h"
namespace hf
{
	KBInput::KBInput() :
		Controller("KBInput")
	{
		AddDependency("SDLEvents");
	}

	bool KBInput::Init()
	{
		m_appManager->AddListener(this, MessageType::KEY_STATE_CHANGE);
		return true;
	}

	bool KBInput::OnMessage(Message &message)
	{
		switch (message.type){
		case MessageType::KEY_STATE_CHANGE:{
			KeyStateChange &keyInput = static_cast<KeyStateChange&>(message);
			switch (keyInput.m_key)
			{
			case Key::ArrowUp:
				if (keyInput.m_isPressed)
					m_states["up"] = true;
				else
					m_states["up"] = false;
				break;
			case Key::ArrowDown:
				if (keyInput.m_isPressed)
					m_states["down"] = true;
				else
					m_states["down"] = false;
				break;
			case Key::ArrowLeft:
				if (keyInput.m_isPressed)
					m_states["left"] = true;
				else
					m_states["left"] = false;
				break;
			case Key::ArrowRight:
				if (keyInput.m_isPressed)
					m_states["right"] = true;
				else
					m_states["right"] = false;
				break;
			case Key::Q:
				if (keyInput.m_isPressed)
					m_states["reset"] = true;
				else
					m_states["reset"] = false;
				break;
			default:
				break;
			}
			return true;
			break;
		}
		default:
			return false;
			break;
		}
	}

	int KBInput::Evaluate()
	{
		return 100;
	}
}