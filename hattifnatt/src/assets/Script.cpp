#include "assets/Script.h"

namespace hf
{

Script::Script(const std::string name, const std::string md5):
Asset(name, "script", "lua", md5)
{

}

bool Script::Prefetch()
{
	if (!Validate())
		return false;

	return true;
}

std::string Script::Get() const
{
	return GetPath();
}

}