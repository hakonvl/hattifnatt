#include "assets/Map.h"
#include <rapidxml/rapidxml.hpp>

#include "Geometry.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <utility>
namespace hf
{

Map::Map(const std::string name, const std::string md5):
Asset(name, "map", "xml", md5)
{

}

bool Map::Prefetch()
{
	if (Validate())
	{
		std::ifstream file;
		try
		{		
			file.open(GetPath());
			if (!file)
			{
				std::string message = "Error opening ";
				message.append(GetPath());
				throw std::ios::failure(message);
			}
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
			return false;
		}
		std::stringstream buffer;

		buffer << file.rdbuf();
		m_xml = buffer.str();
		rapidxml::xml_document<> document;
		std::string documentString = m_xml;
		document.parse<0>(&documentString[0]);
		rapidxml::xml_node<> *rootNode = document.first_node("map");
		
		for (rapidxml::xml_node<> *node = rootNode->first_node("image"); node; node = node->next_sibling("image"))
		{
			std::string name = node->first_attribute("name")->value();
			iRect placement = iRect( iVec3( atoi(node->first_attribute("x")->value()),
				atoi(node->first_attribute("y")->value()),
				atoi(node->first_attribute("z")->value()) ),
				atoi(node->first_attribute("h")->value()),
				atoi(node->first_attribute("w")->value())
			);
			m_images.emplace_back(name, placement);
		}

		for (rapidxml::xml_node<> *node = rootNode->first_node("hit-box"); node; node = node->next_sibling("hit-box"))
		{
		m_hitBoxes.emplace_back(iVec3(atoi(node->first_attribute("x")->value()),
								atoi(node->first_attribute("y")->value()),
								atoi(node->first_attribute("z")->value())),
								atoi(node->first_attribute("h")->value()),
								atoi(node->first_attribute("w")->value()));
		}
		return true;
	}
	else
	{
		return false;
	}
}

const Map::ImageInfoContainer& Map::GetImageInfoContainer() const
{
	return m_images;
}

const Map::HitBoxes& Map::GetHitBoxes() const
{
	return m_hitBoxes;
}
}