#include "assets/XMLDoc.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
namespace hf
{

XMLDoc::XMLDoc(const std::string name, const std::string md5):
Asset(name, "xml", "xml", md5)
{

}

bool XMLDoc::Prefetch()
{
	if (Validate())
	{
		std::ifstream file;
		try
		{		
			file.open(GetPath());
			if (!file)
			{
				std::string message = "Error opening ";
				message.append(GetPath());
				throw std::ios::failure(message);
			}
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
			return false;
		}
		std::stringstream buffer;

		buffer << file.rdbuf();
		m_string = buffer.str();

		return true;
	}
	else
	{
		return false;
	}
}

std::string XMLDoc::GetFileString() const
{
	return m_string;
}
}