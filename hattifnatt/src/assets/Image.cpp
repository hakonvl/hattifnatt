#include "assets/Image.h"
#include <SDL_image.h>
#include <SDL.h>

namespace hf
{

Image::Image(const std::string name, int width, int height, int numFrames, double animationTime, const std::string md5) :
	Asset(name, "image", "png", md5),
	m_isPrefetched(false),
	m_texture(nullptr),
	m_surface(nullptr),
	m_width(width),
	m_height(height),
	m_numFrames(numFrames),
	m_animationTime(animationTime),
	m_currentFrame(0),
	m_lastFrameChange(ImageClock::now())
{

}

Image::Image(const std::string name, int width, int height, int numFrames, double animationTime, RGBA colorKey, const std::string md5) :
	Asset(name, "image", "png", md5),
	m_isPrefetched(false),
	m_texture(nullptr),
	m_surface(nullptr),
	m_width(width),
	m_height(height),
	m_colorKey(colorKey),
	m_numFrames(numFrames),
	m_animationTime(animationTime),
	m_currentFrame(0),
	m_lastFrameChange(ImageClock::now())
{
}
Image::~Image()
{
	SDL_DestroyTexture(m_texture);
}
bool Image::Prefetch()
{
	if (Validate())
	{
		if(m_surface != nullptr)
			return true;
		m_surface = IMG_Load(GetPath().c_str());
		if (m_surface == nullptr)
		{
			throw PrefetchError(GetName(), SDL_GetError());
			return false;
		}
		if (SDL_SetColorKey(m_surface, SDL_TRUE, SDL_MapRGB(m_surface->format, m_colorKey.r, m_colorKey.g, m_colorKey.b) != 0))
		{
			throw PrefetchError(GetName(), SDL_GetError());
			return false;
		}
		if (m_surface == nullptr)
		{
			throw PrefetchError(GetName(), SDL_GetError());
			return false;
		}
		m_isPrefetched = true;
		return true;
	}
	else
	{
		return false;
	}
}

bool Image::PrefetchTexture(SDL_Renderer* renderer)
{
	if (m_texture != nullptr)
		return true;
	try
	{
		Prefetch();
	}
	catch(PrefetchError &e)
	{
		throw e;
		return false;
	}
	m_texture = SDL_CreateTextureFromSurface(renderer, m_surface);
	if (m_texture == nullptr)
	{
		throw PrefetchError(GetName(), SDL_GetError());
		return false;
	}
	return true;
}

int Image::GetHeight() const
{
	return m_height;
}
int Image::GetWidth() const
{
	return m_width;
}

SDL_Texture* Image::GetTexture() const
{
	return m_texture;
}

SDL_Surface* Image::GetSurface() const
{
	return m_surface;
}

bool Image::Render(SDL_Renderer* renderer, iRect rect)
{

	if(m_texture == nullptr)
	{
		throw RenderError(GetName(), "Texture == nullptr");
		return false;
	}
	
	int height{};
	int width{};
	
	if (rect.GetLeft().Length() == 0)
		height = GetHeight();
	else
		height = rect.GetLeft().Length();
	
	if (rect.GetBottom().Length() == 0)
		width = GetWidth();
	else
		width = rect.GetBottom().Length();

	auto now = ImageClock::now();
	std::chrono::duration<double> deltaTime = now - m_lastFrameChange;
	double wantedDelta = m_animationTime / m_numFrames;
	if((deltaTime.count() * 1000.00) > wantedDelta)
	{	
		m_lastFrameChange = ImageClock::now();
		m_currentFrame++;
		if (m_currentFrame >= m_numFrames)
			m_currentFrame = 0;

	}
	SDL_Rect dest { rect.GetA().x, rect.GetA().y, width, height };
	SDL_Rect source { 0, m_currentFrame * height, width, height };
	if (SDL_RenderCopy(renderer, m_texture, &source, &dest) == 0)
		return true;
	else
	{
		throw RenderError(GetName(), SDL_GetError());
		return false;
	}	
}

bool Image::Blit(SDL_Surface* destination, iRect rect)
{
	if(m_surface == nullptr)
	{
		throw RenderError(GetName(), "Surface == nullptr");
		return false;
	}
	int height{};
	int width{};
	
	if (rect.GetLeft().Length() == 0)
		height = GetHeight();
	else
		height = rect.GetLeft().Length();
	
	if (rect.GetBottom().Length() == 0)
		width = GetWidth();
	else
		width = rect.GetBottom().Length();
	
	auto now = ImageClock::now();
	std::chrono::duration<double> deltaTime = now - m_lastFrameChange;
	double wantedDelta = m_animationTime / m_numFrames;
	if((deltaTime.count() * 1000.00) > wantedDelta)
	{	
		m_lastFrameChange = ImageClock::now();
		m_currentFrame++;
		if (m_currentFrame >= m_numFrames)
			m_currentFrame = 0;
		
	}
	SDL_Rect dest { rect.GetA().x, rect.GetA().y, width, height };
	SDL_Rect source { 0, m_currentFrame * height, width, height };
	if (SDL_BlitScaled(m_surface, &source, destination, &dest) == 0)
		return true;
	else
	{
		throw RenderError(GetName(), SDL_GetError());
		return false;
	}	
}


}