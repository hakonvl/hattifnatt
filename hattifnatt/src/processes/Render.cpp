#include "processes/Render.h"
#include "Geometry.h"
#include "IAppManager.h"
#include "assets/Image.h"
#include "messages/WriteMessage.h"
#include "messages/MapUpdate.h"
#include "messages/EntityUpdate.h"
#include "messages/FPSUpdate.h"
#include "messages/GetAsset.h"
#include "messages/GetSetting.h"
#include <string>
#include <SDL.h>
#include <SDL_image.h>

#include <iostream>
namespace hf
{
	Render::Render(bool useWindow) :
		IProcess("Render"),
		m_window(nullptr),
		m_renderer(nullptr),
		m_texture(nullptr),
		m_useWindow(useWindow),
		m_renderHitBoxes(false)
	{

	}

	bool Render::Init()
	{
		m_appManager->AddListener(this, MessageType::MAP_UPDATE);
		m_appManager->AddListener(this, MessageType::ENTITY_UPDATE);
		if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
		{
			std::string string = "SDL_Init Error: ";
			string.append(SDL_GetError());
			WriteMessage message(string);
			m_appManager->SendEvent(message);
			return false;
		}

		//TODO: Whap happens if wheight and wwdidth are out of range??
		GetSetting height("wheight");
		m_appManager->SendEvent(height);
		m_height = atoi(height.GetValue().c_str());
		
		GetSetting width("wwidth");
		m_appManager->SendEvent(width);
		m_width = atoi(width.GetValue().c_str());
		
		GetSetting renderHitBoxes("render-crash-boxes");
		m_appManager->SendEvent(renderHitBoxes);
		if (renderHitBoxes.GetValue() == "true")
			m_renderHitBoxes = true;

		SDL_WindowFlags windowFlags = SDL_WINDOW_SHOWN;
		if(!m_useWindow)
			windowFlags = SDL_WINDOW_HIDDEN;
		m_window = SDL_CreateWindow("Hattifnatt", 100, 100, m_width, m_height, windowFlags);

		if (!m_useWindow)
		{
			m_texture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGB555, SDL_TEXTUREACCESS_STREAMING | SDL_TEXTUREACCESS_TARGET, m_width, m_height);
		}
		
		if (m_window == nullptr)
		{
			std::string string = "SDL_CreateWindow error: ";
			string.append(SDL_GetError());
			WriteMessage message(string);
			m_appManager->SendEvent(message);
			return false;
		}
		

		m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED |SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE );
		if (m_renderer == nullptr)
		{
			std::string string = "SDL_CreateRenderer error: ";
			string.append(SDL_GetError());
			WriteMessage message(string);
			m_appManager->SendEvent(message);
			return false;
		}
		
		SDL_SetRenderDrawColor(m_renderer, 0x00, 0x00, 0x00, 0xff);
		if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG )
		{
			std::string string = "IMG_Init error: ";
			string.append(SDL_GetError());
			WriteMessage message(string);
			m_appManager->SendEvent(message);
			return false;
		}
		
		return true;
	}

	Render::~Render()
	{
		IMG_Quit();
		SDL_DestroyRenderer(m_renderer);
		SDL_DestroyWindow(m_window);
		SDL_Quit();
	}

	void Render::OnLoop()
	{
		SDL_RenderClear(m_renderer);
	
		for (auto it : m_staticTargets)
		{			
			try
			{
				it.image->PrefetchTexture(m_renderer);
				//TODO: Get height and width from map
				try{
					it.image->Render(m_renderer, it.placement);
				}
				catch (RenderError& except)
				{
					WriteMessage msg(except.what());
					m_appManager->SendEvent(msg);
				}
			}
			catch (PrefetchError& except)
			{
				WriteMessage msg(except.what());
				m_appManager->SendEvent(msg);
			}
		}
		for (auto it : m_entityTargets)
		{		
			//TODO: Consider removing this prefetch
			try
			{
				it.image->PrefetchTexture(m_renderer);
				//TODO: Get height and width from map
				try
				{
					it.image->Render(m_renderer, it.placement);
				}
				catch (RenderError& except)
				{
					
					WriteMessage msg(except.what());
					m_appManager->SendEvent(msg);
				}
			}
			catch (PrefetchError& except)
			{
				
				WriteMessage msg(except.what());
				m_appManager->SendEvent(msg);
			}
		}

		SDL_SetRenderDrawColor(m_renderer, 0xFF, 0x00, 0x00, 0x5);
		for (auto it : m_hitBoxiRects)
		{
			SDL_RenderFillRect(m_renderer, &it);
		}
		SDL_SetRenderDrawColor(m_renderer, 0x00, 0x00, 0x00, 0xff);
		
		if(m_useWindow)
		{
			SDL_RenderPresent(m_renderer);
		}
		else
		{
			SDL_SetRenderTarget(m_renderer, m_texture);
			SDL_Rect renderQuad {0, 0, m_width, m_height};
			SDL_RenderCopy(m_renderer, m_texture, NULL, &renderQuad);
		}
	}

	bool Render::OnMessage(Message &message)
	{
		switch (message.type)
		{
		case MessageType::MAP_UPDATE: {
			MapUpdate& mapUpdate = static_cast<MapUpdate&>(message);
			m_staticTargets.clear();
			for (auto& it : mapUpdate.images)
			{
				GetAsset getImage(it.name);
				m_appManager->SendEvent(getImage);
				if (getImage.m_asset != nullptr && getImage.m_asset->GetType().compare("image") == 0)
				{
					Image* image = static_cast<Image*>(getImage.m_asset);
					try
					{
						image->PrefetchTexture(m_renderer);
	
					}
					catch (PrefetchError& e)
					{
						WriteMessage error(e.what());
						m_appManager->SendEvent(error);
					}
					iRect rect = it.placement;
					m_staticTargets.emplace_back(rect, image);
				}
				else
				{
					WriteMessage error("Failed to get asset " + it.name);
					m_appManager->SendEvent(error);
				}
			}
			m_hitBoxiRects.clear();
			for (auto& it : mapUpdate.hitBoxes)
			{
				
				m_hitBoxiRects.emplace_back(SDL_Rect{ it.GetA().x, it.GetA().y, it.GetLeft().y, it.GetTop().x });
			}
			return true;
			break;
		}
		case MessageType::ENTITY_UPDATE:{
			EntityUpdate& update = static_cast<EntityUpdate&>(message);
			m_entityTargets.clear();
			for (auto& it : update.entityContainer)
			{
				GetAsset getImage(it.GetImageName());
				m_appManager->SendEvent(getImage);
				if (getImage.m_asset != nullptr && getImage.m_asset->GetType().compare("image") == 0)
				{
					Image* image = static_cast<Image*>(getImage.m_asset);
					try
					{
						image->PrefetchTexture(m_renderer);

					}
					catch (PrefetchError& e)
					{
						WriteMessage error(e.what());
						m_appManager->SendEvent(error);
						return false;
					}
					iRect rect = it.GetPosition();
					m_entityTargets.emplace_back(RenderTarget{ rect, image });
				}
				else
				{
					WriteMessage error("Failed to get asset " + it.GetImageName());
					m_appManager->SendEvent(error);
				}
			}
			return true;
			break;
		}
		default:
			return false;
			break;
		}
		
	}

	SDL_Texture* Render::GetTexture()
	{
		return m_texture;
	}
}
