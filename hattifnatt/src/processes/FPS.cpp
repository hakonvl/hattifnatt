#include "IAppManager.h"
#include "processes/FPS.h"
#include "messages/FPSUpdate.h"
#include "messages/GetSetting.h"
#include "messages/WriteMessage.h"
#include <SDL.h>

namespace hf
{
FPS::FPS(): 
    IProcess("FPS"),
    m_FPSCapState(true),
    m_lastRun(FPSClock::now()),
    m_numFrames(0),
    m_FPSCap(0)
{

}


void FPS::OnLoop()
{
    // Calcualate wanted delta time and current delta time. If we got more time, then we can sleep for the remaining time.
    // Remember to relculate deltaTime after sleeping, so we can get the correct fps value
    FPSClock::time_point now = FPSClock::now();
    std::chrono::duration<double> deltaTime = now - m_lastRun;
    double wantedDelta = 1.00 /m_FPSCap;

    if(m_FPSCapState && m_numFrames > 1 && deltaTime.count() < wantedDelta)
    {
        double sleep = (wantedDelta - deltaTime.count()) * 1000.00;

        SDL_Delay(sleep);
    }
    else{
        SDL_Delay(10);
    }
    
    // Recalculate deltaTime and then calculate fps
    now = FPSClock::now();
    deltaTime = now - m_lastRun;
    m_fps = 1.00 / deltaTime.count();

    m_numFrames++;
    m_lastRun = FPSClock::now();
    
    FPSUpdate update(m_fps, deltaTime.count() * 1000.00);
    m_appManager->SendEvent(update);
}

bool FPS::Init()
{
    GetSetting fpsCap("fps-cap");
    m_appManager->SendEvent(fpsCap);
    m_FPSCap = atoi(fpsCap.GetValue().c_str());
    std::string message = "Running at ";
    message.append(fpsCap.GetValue());
    message.append(" FPS");

    WriteMessage write(message);
    m_appManager->SendEvent(write);


    return true;
}

void FPS::SetFPSCapState(bool state)
{
    m_FPSCapState = state;
}

bool FPS::GetFPSCapState()
{
    return m_FPSCapState;
}

void FPS::SetFPSCap(int fps)
{
    m_FPSCap = fps;
}

int FPS::GetFpsCap()
{
    return m_FPSCap;
}
}
