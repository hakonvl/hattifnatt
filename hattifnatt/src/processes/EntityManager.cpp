#include "processes/EntityManager.h"
#include "messages/EntityUpdate.h"
#include "messages/WriteMessage.h"
#include "messages/FPSUpdate.h"
#include "messages/MapUpdate.h"
#include "messages/GetLuaState.h"
#include "IAppManager.h"
#include "Geometry.h"
#include "LuaIncludes.h"
#include <algorithm>

namespace hf
{
	EntityManager::EntityManager() : IProcess("EntityManager"),
		m_speedFactor(1)
	{
		AddDependency("FPS");
		AddDependency("LuaBridge");
	}


	void EntityManager::OnLoop()
	{
		for (auto& it : m_entities)
		{
			it.Think(m_speedFactor);
			it.m_position += it.m_speed;
			
		}
		EntityUpdate update(m_entities);
		m_appManager->SendEvent(update);
	}

	bool EntityManager::Init()
	{
		m_appManager->AddListener(this, MessageType::FPS_UPDATE);
		m_appManager->AddListener(this, MessageType::MAP_UPDATE);

		GetLuaState getState;
		m_appManager->SendEvent(getState);
		if (getState.m_L == nullptr)
		{
			WriteMessage msg("EntityManager could not get lua state in Init()");
			m_appManager->SendEvent(msg);
			return false;
		}
		luabridge::getGlobalNamespace(getState.m_L)
			.beginNamespace("hf")
				.beginClass<Entity>("Entity")
					.addConstructor<void(*)(std::string)>()
					.addProperty("name", &Entity::GetName)
					.addProperty("position", &Entity::GetPosition, &Entity::SetPosistion)
					.addProperty("speed", &Entity::GetSpeed, &Entity::SetSpeed)
					.addProperty("accel", &Entity::GetAccel, &Entity::SetAccel)
					.addProperty("imageName", &Entity::GetImageName, &Entity::SetImageName)
				.endClass()
			 .endNamespace();
			
		luabridge::getGlobalNamespace(getState.m_L)
			.beginNamespace("hf")
				.beginClass<EntityManager>("EntityManager")
					.addConstructor<void(*)()>()
					.addFunction("AddEntity", &EntityManager::AddEntity)
					.addFunction("AddSkeleton", &EntityManager::AddSkeleton)
				.endClass()
			.endNamespace();
		
		luabridge::push(getState.m_L, this);
		lua_setglobal(getState.m_L, "entityManager");
		return true;
	}

	bool EntityManager::OnMessage(Message &message)
	{
		switch (message.type){
		case MessageType::FPS_UPDATE:{
			FPSUpdate& update = static_cast<FPSUpdate&>(message);
			m_speedFactor = update.GetDelta() / 100.00;
			return true;
			break;
		}
		case MessageType::MAP_UPDATE:{
			MapUpdate& update = static_cast<MapUpdate&>(message);
			m_hitBoxes.clear();
			m_hitBoxes = update.hitBoxes;
			return true;
			break;
		}
		}
	}

	Entity* EntityManager::AddEntity(const std::string& name)
	{
		try{
			m_entities.push_back(Entity(m_skeletons.at(name)));
			return &m_entities.back();
		}
		catch(std::out_of_range& e)
		{
			WriteMessage msg("No entity called " + name);
			m_appManager->SendEvent(msg);
			return nullptr;
		}	
	}
	Entity* EntityManager::AddSkeleton(const std::string name)
	{
		m_skeletons.emplace(name, name);
		return &m_skeletons.at(name);
	}
}