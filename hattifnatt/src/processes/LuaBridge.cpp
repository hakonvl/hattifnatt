#include "processes/LuaBridge.h"
#include "messages/RunScript.h"
#include "messages/WriteMessage.h"
#include "messages/GetLuaState.h"
#include "IAppManager.h"
#include "Geometry.h"
#include <iostream>
#include <string>
namespace hf
{

	LuaBridge::LuaBridge() : IProcess("LuaBridge")
	{}

	bool LuaBridge::Init()
	{
		m_L = luaL_newstate();
		
		luaL_openlibs(m_L);

		m_appManager->AddListener(this, MessageType::RUN_SCRIPT);
		m_appManager->AddListener(this, MessageType::GET_LUA_STATE);

		//Expose various data types. First most normal template variants of Vector
		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
			   .beginClass<iVec3>("iVec3")
					.addConstructor<void(*)(int, int, int)>()
					.addData("x", &iVec3::x)
					.addData("y", &iVec3::y)
					.addData("z", &iVec3::z)
					.addFunction("+", &iVec3::operator+<int>)
					.addFunction("-", &iVec3::operator-<int>)
					.addFunction("*", &iVec3::operator*<int>)
					.addFunction("/", &iVec3::operator/<int>)
					.addProperty("Length", &iVec3::Length)
				.endClass()
			.endNamespace();

		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<dVec3>("dVec3")
					.addConstructor<void(*)(double, double, double)>()
					.addData("x", &dVec3::x)
					.addData("y", &dVec3::y)
					.addData("z", &dVec3::z)
					.addFunction("+", &dVec3::operator+<double>)
					.addFunction("-", &dVec3::operator-<double>)
					.addFunction("*", &dVec3::operator*<double>)
					.addFunction("/", &dVec3::operator/<double>)
					.addProperty("Length", &dVec3::Length)
				.endClass()
			.endNamespace();
	   
		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<uiVec3>("uiec3")
					.addConstructor<void(*)(unsigned int, unsigned int, unsigned int)>()
					.addData("x", &uiVec3::x)
					.addData("y", &uiVec3::y)
					.addData("z", &uiVec3::z)
					.addFunction("+", &uiVec3::operator+<unsigned int>)
					.addFunction("-", &uiVec3::operator-<unsigned int>)
					.addFunction("*", &uiVec3::operator*<unsigned int>)
					.addFunction("/", &uiVec3::operator/<unsigned int>)
					.addProperty("Length", &uiVec3::Length)
				.endClass()
			.endNamespace();

		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<fVec3>("fVec3")
					.addConstructor<void(*)(float, float, float)>()
					.addData("x", &fVec3::x)
					.addData("y", &fVec3::y)
					.addData("z", &fVec3::z)
					.addFunction("+", &fVec3::operator+<float>)
					.addFunction("-", &fVec3::operator-<float>)
					.addFunction("*", &fVec3::operator*<float>)
					.addFunction("/", &fVec3::operator/<float>)
					.addProperty("Length", &fVec3::Length)
				.endClass()
			.endNamespace();

		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<iTetra>("iTetra")
					.addConstructor<void(*)(iVec3, iVec3, iVec3, iVec3)>()
					.addProperty("a", &iTetra::GetA)
					.addProperty("b", &iTetra::GetB)
					.addProperty("c", &iTetra::GetC)
					.addProperty("d", &iTetra::GetD)
					.addProperty("left", &iTetra::GetLeft)
					.addProperty("right",&iTetra::GetRight)
					.addProperty("top", &iTetra::GetTop)
					.addProperty("bottom", &iTetra::GetBottom)
					.addFunction("+", &iTetra::operator+<iVec3>)
					.addFunction("-", &iTetra::operator-<iVec3>)
				.endClass()
				.deriveClass<iRect, iTetra>("iRect")
					.addConstructor<void(*)(iVec3, int, int)>()
				.endClass()
			.endNamespace();

		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<uiTetra>("uiTetra")
					.addConstructor<void(*)(uiVec3, uiVec3, uiVec3, uiVec3)>()
					.addProperty("a", &uiTetra::GetA)
					.addProperty("b", &uiTetra::GetB)
					.addProperty("c", &uiTetra::GetC)
					.addProperty("d", &uiTetra::GetD)
					.addProperty("left", &uiTetra::GetLeft)
					.addProperty("right", &uiTetra::GetRight)
					.addProperty("top", &uiTetra::GetTop)
					.addProperty("bottom", &uiTetra::GetBottom)
					.addFunction("+", &uiTetra::operator+<uiVec3>)
					.addFunction("-", &uiTetra::operator-<uiVec3>)
				.endClass()
				.deriveClass<uiRect, uiTetra>("uiRect")
					.addConstructor<void(*)(uiVec3, unsigned int, unsigned int)>()
				.endClass()
			.endNamespace();

		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<fTetra>("fTetra")
					.addConstructor<void(*)(fVec3, fVec3, fVec3, fVec3)>()
					.addProperty("a", &fTetra::GetA)
					.addProperty("b", &fTetra::GetB)
					.addProperty("c", &fTetra::GetC)
					.addProperty("d", &fTetra::GetD)
					.addProperty("left", &fTetra::GetLeft)
					.addProperty("right", &fTetra::GetRight)
					.addProperty("top", &fTetra::GetTop)
					.addProperty("bottom", &fTetra::GetBottom)
					.addFunction("+", &fTetra::operator+<fVec3>)
					.addFunction("-", &fTetra::operator-<fVec3>)
				.endClass()
				.deriveClass<fRect, fTetra>("fRect")
					.addConstructor<void(*)(fVec3, float, float)>()
				.endClass()
			.endNamespace();

		luabridge::getGlobalNamespace(m_L)
			.beginNamespace("hf")
				.beginClass<dTetra>("dTetra")
					.addConstructor<void(*)(dVec3, dVec3, dVec3, dVec3)>()
					.addProperty("a", &dTetra::GetA)
					.addProperty("b", &dTetra::GetB)
					.addProperty("c", &dTetra::GetC)
					.addProperty("d", &dTetra::GetD)
					.addProperty("left", &dTetra::GetLeft)
					.addProperty("right", &dTetra::GetRight)
					.addProperty("top", &dTetra::GetTop)
					.addProperty("bottom", &dTetra::GetBottom)
					.addFunction("+", &dTetra::operator+<dVec3>)
					.addFunction("-", &dTetra::operator-<dVec3>)
				.endClass()
				.deriveClass<dRect, dTetra>("dRect")
					.addConstructor<void(*)(dVec3, double, double)>()
				.endClass()
			.endNamespace();
		return true;
	

	}

	bool LuaBridge::OnMessage(Message &message)
	{
		switch (message.type){
			case MessageType::RUN_SCRIPT:
			{
				RunScript &scriptMsg = static_cast<RunScript&>(message);
				int s = luaL_loadfile(m_L, scriptMsg.script.Get().c_str());
				if (!s)
					s = lua_pcall(m_L, 0, LUA_MULTRET, 0);
	
				//show any errors
				if (s){
					std::string error = "LuaError: ";
					error.append(lua_tostring(m_L, -1));
					WriteMessage message{error};
					m_appManager->SendEvent(message);
					lua_pop(m_L, 1);
				}
				break;
			}	
			case MessageType::GET_LUA_STATE:
			{
				GetLuaState& getState = static_cast<GetLuaState&>(message);
				getState.m_L = m_L;
				break;
			}
			default:
				return false;
				break;
			}		
		return false;
	}
}
