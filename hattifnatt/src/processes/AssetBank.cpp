#include "processes/AssetBank.h"
#include "Asset.h"
#include "messages/GetAsset.h"
#include "messages/GetAssets.h"
#include "IAppManager.h"
namespace hf{

	AssetBank::AssetBank() : IProcess("AssetBank")
	{

	}

	bool AssetBank::OnMessage(Message& message)
	{
		switch (message.type)
		{
			case MessageType::GET_ASSET:
			{
				GetAsset &getAsset = static_cast<GetAsset&>(message);
				getAsset.m_asset = m_assetMap[getAsset.m_name];
	
				return true;
				break;
			}
			case MessageType::GET_ASSETS:
			{
				GetAssets &getAssets = static_cast<GetAssets&>(message);
				getAssets.m_assetMap = GetAssetMap();
				return true;
				break;
			}
		}
		return false;
	}
	
	const AssetMap* AssetBank::GetAssetMap() const
	{
		return &m_assetMap;
	}

	void AssetBank::AddAsset(Asset *asset)
	{
		m_assetMap[asset->GetName()] = asset;


	}

	bool AssetBank::Init()
	{
		m_appManager->AddListener(this, MessageType::GET_ASSET);
		m_appManager->AddListener(this, MessageType::GET_ASSETS);
		return true;
	}

	void AssetBank::OnLoop()
	{
	
	}


}