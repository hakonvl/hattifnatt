#include "IAppManager.h"
#include "processes/Log.h"
#include "messages/WriteMessage.h"
#include "messages/GetSetting.h"
#include <iostream>
#include <time.h>
#include <string>

namespace hf
{
Log::Log():
IProcess("Log"),
m_defaultLogFile("logs/default.log"),
m_debugLogFile("logs/debug.log"),
m_debugState(false)
{
	//AddDependency("settings");
}

Log::~Log()
{
	Write(static_cast<std::string>("LOG SESSION CLOSED"));
	m_defaultLog.close();
	
	SetDebugState(false);
}

bool Log::Init()
{
	m_defaultLog.open(m_defaultLogFile);
	if(m_defaultLog.is_open())
	{
		Write(static_cast<std::string>("NEW LOG SESSION OPENED"));
	}
	else
	{
		return false;
	}

	GetSetting debugState("debug-state");
	m_appManager->SendEvent(debugState);
	
	if (debugState.GetValue().compare("true") == 0)
	{
		SetDebugState(true);
	}

	m_appManager->AddListener(this, MessageType::WRITE_MESSAGE);
	return true;
}

void Log::OnLoop()
{

}

bool Log::OnMessage(Message &message)
{
	if (message.type == MessageType::WRITE_MESSAGE)
	{
		WriteMessage &msg = static_cast<WriteMessage&>(message);
		if (!msg.m_debug)
		{
			Write(msg.m_message);
			return true;
		}
		else
		{
			WriteDebug(msg.m_message);
			return true;
		}
	}

	return false;
}
void Log::Write(std::string message)
{
	//TODO What happens if the fstream is closed?
	time_t currentTime = time(0);
	
	std::string currentTimeString = ctime(&currentTime);
	m_defaultLog << currentTimeString.substr(0, currentTimeString.length() -1) << ": " << message << std::endl;
	std::cout << currentTimeString.substr(0, currentTimeString.length() -1) << ": " << message << std::endl;
}


void Log::WriteDebug(std::string message)
{

	time_t currentTime = time(0);
	
	std::string currentTimeString = ctime(&currentTime);
	m_debugLog << currentTimeString.substr( 0, currentTimeString.length() -1) << ": " << message << std::endl;
	std::cout << "Debug "<< currentTimeString.substr( 0, currentTimeString.length() -1) << ": " << message << std::endl;

}

bool Log::SetDebugState(bool state)
{
	if(state && !m_debugState){
			m_debugLog.open(m_debugLogFile);
			if(m_debugLog.is_open())
			{
				WriteDebug((std::string)"NEW DEBUG LOG SESSION OPENED");
				m_debugState = true;
				return true;
			}
			else
			{
				Write("Failed to open debug log");
				return false;
			}
	}
	else if (!state && m_debugState)
	{
			WriteDebug((std::string)"DEBUG LOG SESSION CLOSED");
			m_debugState = false;
			m_debugLog.close();
			return true;
	}
	return false;
}
}
