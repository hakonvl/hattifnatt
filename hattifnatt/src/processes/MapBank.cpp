#include "processes/MapBank.h"
#include "IAppManager.h"
#include "assets/XMLDoc.h"
#include "messages/WriteMessage.h"
#include "messages/GetAssets.h"
#include "messages/MapUpdate.h"
#include "rapidxml/rapidxml.hpp"
#include "processes/AssetBank.h"
#include <list>
#include <algorithm>

#include <iostream>
namespace hf
{
MapBank::MapBank(): IProcess("mapBank")
{
	AddDependency("AssetBank");

}

bool MapBank::Init()
{


	/*
	GetAsset mapListAsset = GetAsset("mapList");
	m_appManager->SendEvent(mapListAsset);
	XMLDoc* mapList = static_cast<XMLDoc*>(mapListAsset.m_asset);
	// Load the map list

	// Remove the input pointer in GetAsset, and add a GetAsset() method, and then do some casting

	if (mapList == nullptr)
	{
		WriteMessage error("Failed to get mapList asset, aborting process");
		m_appManager->SendEvent(error);
		return false;
	}
	if (!mapList->Prefetch())
	{
		WriteMessage error("Failed to prefetch the mapList, aborting process");
		m_appManager->SendEvent(error);
		return false;
	}

	// Parse it

	rapidxml::xml_document<> document;
	std::string documentString = mapList->GetFileString();
	document.parse<0>(&documentString[0]);
	rapidxml::xml_node<> *rootNode = document.first_node("maps");

	std::string tmpMap;
	for (rapidxml::xml_node<> *node = rootNode->first_node("map"); node; node = node->next_sibling())
	{
		tmpMap = node->first_attribute("id")->value();
		m_mapList.push_front(tmpMap);
	}*/

	GetAssets getAssets = GetAssets();
	m_appManager->SendEvent(getAssets);
	
	std::for_each(getAssets.GetAssetMap()->begin(), getAssets.GetAssetMap()->end(), [=](std::pair<std::string, Asset*> pair){ 
		if (pair.second->GetType().compare("map") == 0)
		{
			m_mapList[pair.first] = static_cast<Map*>(pair.second);
		} 
	});
	return true;
}

void MapBank::OnLoop()
{
}

bool MapBank::Load(std::string name)
{
	Map *currentMap = m_mapList[name];
	if (currentMap != nullptr && currentMap->Prefetch())
	{
		m_currentMap = m_mapList[name];
		SendMapUpdate();
		return true;
	}
	else
	{
		return false;
	}
}


void MapBank::SendMapUpdate()
{
	MapUpdate event(m_currentMap->GetImageInfoContainer(), m_currentMap->GetHitBoxes());

	m_appManager->SendEvent(event);
}


}