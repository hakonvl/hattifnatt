#include "IAppManager.h"
#include "processes/SDLEvents.h"
#include <string>
#include <SDL.h>
#include "messages/Quit.h"
#include "messages/KeyStateChange.h"
namespace hf
{
	
SDLEvents::SDLEvents():
IProcess("SDLEvents")
{

}



bool SDLEvents::Init()
{

	return true;
}

void SDLEvents::OnLoop()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
		{
			Quit quitMessage{};
			m_appManager->SendEvent(quitMessage);
			break;
		}
		case SDL_KEYDOWN:
		{
			KeyStateChange stateChange { SDLKeyToHFKey(event.key.keysym.sym), true };
			m_appManager->SendEvent(stateChange);
			break;
		}
		case SDL_KEYUP:
		{
			KeyStateChange stateChange { SDLKeyToHFKey(event.key.keysym.sym), false };
			m_appManager->SendEvent(stateChange);
			break;
		}
		}
	}

}

bool SDLEvents::OnMessage(Message &message)
{

	return false;
}

Key SDLEvents::SDLKeyToHFKey(SDL_Keycode key)
{
	Key newKey = Key::NoKey;
	switch (key)
	{
	case SDLK_0:
		newKey = Key::Zero;
		break;
	case SDLK_1:
		newKey = Key::One;
		break;
	case SDLK_2:
		newKey = Key::Two;
		break;
	case SDLK_3:
		newKey = Key::Three;
		break;
	case SDLK_4:
		newKey = Key::Four;
		break;
	case SDLK_5:
		newKey = Key::Five;
		break;
	case SDLK_6:
		newKey = Key::Six;
		break;
	case SDLK_7:
		newKey = Key::Seven;
		break;
	case SDLK_8:
		newKey = Key::Eight;
		break;
	case SDLK_9:
		newKey = Key::Nine;
		break;
	case SDLK_KP_0:
		newKey = Key::NumPadZero;
		break;
	case SDLK_KP_1:
		newKey = Key::NumPadOne;
		break;
	case SDLK_KP_2:
		newKey = Key::NumPadTwo;
		break;
	case SDLK_KP_3:
		newKey = Key::NumPadThree;
		break;
	case SDLK_KP_4:
		newKey = Key::NumPadFour;
		break;
	case SDLK_KP_5:
		newKey = Key::Five;
		break;
	case SDLK_KP_6:
		newKey = Key::NumPadSix;
		break;
	case SDLK_KP_7:
		newKey = Key::NumPadSeven;
		break;
	case SDLK_KP_8:
		newKey = Key::NumPadEight;
		break;
	case SDLK_KP_9:
		newKey = Key::NumPadNine;
		break;
	case SDLK_a:
		newKey = Key::A;
		break;
	case SDLK_b:
		newKey = Key::B;
		break;
	case SDLK_c:
		newKey = Key::C;
		break;
	case SDLK_d:
		newKey = Key::D;
		break;
	case SDLK_e:
		newKey = Key::E;
		break;
	case SDLK_f:
		newKey = Key::F;
		break;
	case SDLK_g:
		newKey = Key::G;
		break;
	case SDLK_h:
		newKey = Key::H;
		break;
	case SDLK_i:
		newKey = Key::I;
		break;
	case SDLK_j:
		newKey = Key::J;
		break;
	case SDLK_k:
		newKey = Key::K;
		break;
	case SDLK_l:
		newKey = Key::L;
		break;
	case SDLK_m:
		newKey = Key::M;
		break;
	case SDLK_n:
		newKey = Key::N;
		break;
	case SDLK_o:
		newKey = Key::O;
		break;
	case SDLK_p:
		newKey = Key::P;
		break;
	case SDLK_q:
		newKey = Key::Q;
		break;
	case SDLK_r:
		newKey = Key::R;
		break;
	case SDLK_s:
		newKey = Key::S;
		break;
	case SDLK_t:
		newKey = Key::T;
		break;
	case SDLK_u:
		newKey = Key::U;
		break;
	case SDLK_v:
		newKey = Key::V;
		break;
	case SDLK_w:
		newKey = Key::W;
		break;
	case SDLK_x:
		newKey = Key::X;
		break;
	case SDLK_y:
		newKey = Key::Y;
		break;
	case SDLK_z:
		newKey = Key::Z;
		break;
	case SDLK_UP:
		newKey = Key::ArrowUp;
		break;
	case SDLK_DOWN:
		newKey = Key::ArrowDown;
		break;
	case SDLK_LEFT:
		newKey = Key::ArrowLeft;
		break;
	case SDLK_RIGHT:
		newKey = Key::ArrowRight;
		break;
	}
	return newKey;
}
}
