#include "processes/Settings.h"
#include "messages/GetSetting.h"
#include "rapidxml/rapidxml.hpp"
#include "IAppManager.h"
#include <iostream>
#include <fstream>
#include <sstream>
namespace hf
{

Settings::Settings():
IProcess("Settings")
{
	
}

bool Settings::Init()
{
	rapidxml::xml_document<> projectsDocument;
	std::ifstream projectsFile;
	std::string projectsString;
	
	try
	{
		projectsFile.open("settings.xml");
		if( !projectsFile ) throw std::ios::failure( "Error opening settings.xml file!" ) ;
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return false;
	}
	std::stringstream buffer;
	buffer << projectsFile.rdbuf();
	projectsString = buffer.str();
	
	projectsDocument.parse<0>(&projectsString[0]);
	
	rapidxml::xml_node<> *rootNode = projectsDocument.first_node("settings");
	
	for(rapidxml::xml_node<> *node=rootNode->first_node("setting"); node; node=node->next_sibling())
	{
		m_settings[node->first_attribute("key")->value()] = node->first_attribute("value")->value();
	}

	m_appManager->AddListener(this, MessageType::GET_SETTING);
	return true;
}

std::string Settings::GetValue(std::string key)
{
	return m_settings[key];
}

void Settings::SetValue(std::string key, std::string value)
{
	m_settings[key] = value;
}

bool Settings::OnMessage(Message &message)
{
	switch (message.type){
	case MessageType::GET_SETTING:
		GetSetting &getSetting = static_cast<GetSetting&>(message);
		getSetting.m_value = GetValue(getSetting.GetKey());
		return true;
		break;

	}
	return false;
}
}
