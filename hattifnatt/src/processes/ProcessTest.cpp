#include "processes/ProcessTest.h"
#include "messages/GetSetting.h"
#include "IAppManager.h"
namespace hf
{
ProcessTest::ProcessTest(): 
	IProcess("ProcessTest")
{

}

void ProcessTest::OnLoop()
{	
	//const Message event(MessageType::TEST);

	GetSetting event("fps-update");
	m_appManager->SendEvent(event);
	
}

bool ProcessTest::OnMessage(Message &message)
{
	return true;
}

bool ProcessTest::Init()
{
	m_appManager->AddListener(this, MessageType::TEST);
	return true;
}
}
