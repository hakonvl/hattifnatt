set(hf_src_dir "${PROJECT_SOURCE_DIR}/hattifnatt/src")
set(hf_inc_dir "${PROJECT_SOURCE_DIR}/hattifnatt/include/hf")

# Variables for all source and header files

# Processes
set(process_sources 
	${hf_src_dir}/processes/ProcessTest.cpp 
	${hf_src_dir}/processes/Log.cpp
	${hf_src_dir}/processes/Settings.cpp
	${hf_src_dir}/processes/FPS.cpp
	${hf_src_dir}/processes/AssetBank.cpp
	${hf_src_dir}/processes/LuaBridge.cpp
	${hf_src_dir}/processes/EntityManager.cpp
	${hf_src_dir}/processes/MapBank.cpp
	${hf_src_dir}/processes/Render.cpp
	${hf_src_dir}/processes/SDLEvents.cpp
)
source_group("Source Files\\Processes" FILES ${process_sources})

set(process_headers
	${hf_inc_dir}/processes/ProcessTest.h
	${hf_inc_dir}/processes/Log.h
	${hf_inc_dir}/processes/Settings.h
	${hf_inc_dir}/processes/FPS.h
	${hf_inc_dir}/processes/AssetBank.h
	${hf_inc_dir}/processes/LuaBridge.h
	${hf_inc_dir}/processes/EntityManager.h
	${hf_inc_dir}/processes/MapBank.h
	${hf_inc_dir}/processes/Render.h
	${hf_inc_dir}/processes/SDLEvents.h
)
source_group("Header Files\\Processes" FILES ${process_headers})

# Controllers and AIs
set(controller_sources
	${hf_src_dir}/controllers/KBInput.cpp
)
source_group("Source Files\\Controllers" FILES ${controller_sources})

set(controller_headers
	${hf_inc_dir}/controllers/KBInput.h
)
source_group("Header Files\\Controllers" FILES ${controller_headers})

# Assets
set(asset_sources 
	${hf_src_dir}/assets/Script.cpp
	${hf_src_dir}/assets/XMLDoc.cpp
	${hf_src_dir}/assets/Map.cpp
	${hf_src_dir}/assets/Image.cpp
)
source_group("Source Files\\Assets" FILES ${asset_sources})

set(asset_headers
	${hf_inc_dir}/assets/Script.h
	${hf_inc_dir}/assets/XMLDoc.h
	${hf_inc_dir}/assets/Map.h
	${hf_inc_dir}/assets/Image.h
)
source_group("Header Files\\Assets" FILES ${asset_headers})

set(sources
	${hf_src_dir}/Asset.cpp
	${hf_src_dir}/Entity.cpp
	${hf_src_dir}/IAppManager.cpp
	${hf_src_dir}/Geometry.cpp
	${hf_src_dir}/Message.cpp
	${hf_src_dir}/IProcess.cpp
	${hf_src_dir}/Controller.cpp
	${hf_src_dir}/Goals.cpp
)
source_group("Source Files" FILES ${sources})

set(headers
	${hf_inc_dir}/Asset.h
	${hf_inc_dir}/Entity.h
	${hf_inc_dir}/IAppManager.h
	${hf_inc_dir}/Message.h
	${hf_inc_dir}/IProcess.h
	${hf_inc_dir}/MessageType.h
	${hf_inc_dir}/Geometry.h
	${hf_inc_dir}/Listener.h
	${hf_inc_dir}/Keys.h
	${hf_inc_dir}/Controller.h
	${hf_inc_dir}/RenderTarget.h
	${hf_inc_dir}/LuaIncludes.h
	${hf_inc_dir}/Goals.h
)
source_group("Header Files" FILES ${headers})

set(message_headers
	${hf_inc_dir}/messages/EntityUpdate.h 
	${hf_inc_dir}/messages/FPSUpdate.h 
	${hf_inc_dir}/messages/GetAsset.h
	${hf_inc_dir}/messages/GetAssets.h
	${hf_inc_dir}/messages/WriteMessage.h
	${hf_inc_dir}/messages/GetSetting.h
	${hf_inc_dir}/messages/MapUpdate.h
	${hf_inc_dir}/messages/Quit.h
	${hf_inc_dir}/messages/KeyStateChange.h
	${hf_inc_dir}/messages/RunScript.h
	${hf_inc_dir}/messages/GetLuaState.h
)
source_group("Messsages" FILES ${message_headers})

# Build all binaries

include_directories(${hf_inc_dir})

add_library(hattifnatt ${sources} ${asset_sources} ${process_sources} ${controller_sources} ${headers} ${asset_headers} ${process_headers} ${controller_headers} ${message_headers})

target_link_libraries(hattifnatt ${SDL2_LIBRARY} ${SDLMAIN2_LIBRARY} ${SDL2IMAGE_LIBRARY} ${LUA_LIBRARY})