#!/bin/sh
echo Updating assets
mkdir -p ../debug/logs
cp -rv ../gfx ../debug/
cp -rv ../misc/* ../debug
cp -rv ../scripts ../debug/

mkdir -p ../release/logs
cp -rv ../gfx/ ../release/
cp -rv ../misc/* ../release
cp -rv ../scripts/ ../release/

