#include "GameManager.h"
#include <string>
#include <hf/Geometry.h>
#include <hf/controllers/KBInput.h>
#include <hf/messages/RunScript.h>

GameManager::GameManager(int argc, char** argv) :
AppManager(argc, argv),
m_mTestMap1("testMap1"),
m_mTestMap2("testMap2"),
m_sHelloWorld("helloWorld"),
m_testEntity(hf::iRect( hf::iVec3(0, 0, 0), 200, 100 )),
m_testImage1("testImage1", 200, 100, 2, 1000, hf::RGBA{ 0xff, 0xff, 0xff }),
m_testEntityImage("testEntityImage", 200, 100, 1, 10000, hf::RGBA{ 0xf0, 0xff, 0xff })
	//m_processesTest(this)
{
}

bool GameManager::OnInit()
{
	//GetLog()->SetDebugState(true);

	m_testImage1.Prefetch();
	m_testEntityImage.Prefetch();

	m_assetBank.AddAsset(&m_mTestMap1);
	m_assetBank.AddAsset(&m_mTestMap2);
	m_assetBank.AddAsset(&m_testImage1);
	m_assetBank.AddAsset(&m_testEntityImage);
	m_assetBank.AddAsset(&m_sHelloWorld);

	hf::Entity* skeleton = m_entityManager.AddSkeleton("testEntity");
	skeleton->SetImageName("testEntityImage");
	skeleton->SetPosistion(hf::iRect(hf::iVec3{0, 0, 0}, 200, 100));
	skeleton->SetSpeed(hf::dVec3(0, 0, 0));
	skeleton->SetAccel(hf::dVec3(0, 0, 0));
	m_mapBank.Load("testMap1");
	m_entityManager.AddEntity("testEntity");
	
	m_sHelloWorld.Prefetch();
	hf::RunScript helloWorld{ m_sHelloWorld };
	SendEvent(helloWorld);


	return true; 
}
