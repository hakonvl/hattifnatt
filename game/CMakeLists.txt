project(Game)
set("game_src_dir" "${PROJECT_SOURCE_DIR}/src")
set("game_inc_dir" "${PROJECT_SOURCE_DIR}/include/game")
set("game_asset_dir" "${PROJECT_SOURCE_DIR}/assets")
set("game_sources"
	${game_src_dir}/GameManager.cpp
	${game_src_dir}/TestEntity.cpp
)
source_group("Source files" FILES ${game_sources})

set("game_headers"
	${game_inc_dir}/GameManager.h
	${game_inc_dir}/TestEntity.h
)
source_group("Messsages" FILES ${game_assets})

set("game_assets"
	${game_asset_dir}/settings.xml
)
source_group("Assets" FILES ${game_assets})

set("game_assets_images"
	${game_asset_dir}/image/testEntityImage.png
	${game_asset_dir}/image/testImage1.png
)
source_group("Assets\\Images" FILES ${game_assets_images})

set("game_assets_scripts"
	${game_asset_dir}/script/helloWorld.lua
)
source_group("Assets\\Scripts" FILES ${game_assets_scripts})


set("game_assets_maps"
	${game_asset_dir}/map/testMap1.xml
)
source_group("Assets\\Maps" FILES ${game_assets_maps})

include_directories(${game_inc_dir})

#Copy assets to bin dir
if(CMAKE_CONFIGURATION_TYPES)
	foreach(i ${CMAKE_CONFIGURATION_TYPES})
		file(COPY "${game_asset_dir}/" DESTINATION "${EXECUTABLE_OUTPUT_PATH}/${i}" )
	endforeach()
else()
	file(COPY "${game_asset_dir}/" DESTINATION "${EXECUTABLE_OUTPUT_PATH}" )
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
add_library(game ${game_headers} ${game_sources} ${game_assets} ${game_assets_images} ${game_assets_maps} ${game_assets_scripts})
target_link_libraries(game hattifnatt)