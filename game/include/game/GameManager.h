#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include <hf/IAppManager.h>
#include <hf/assets/XMLDoc.h>
#include <hf/assets/Map.h>
#include <hf/assets/Image.h>
#include <hf/assets/Script.h>
#include <hf/processes/AssetBank.h>
#include <hf/processes/Settings.h>
#include <hf/processes/Log.h>
#include <hf/processes/FPS.h>
#include <hf/processes/MapBank.h>
#include <hf/processes/Render.h>
#include <hf/processes/SDLEvents.h>
#include <hf/processes/EntityManager.h>
#include <hf/processes/LuaBridge.h>
#include <hf/processes/Settings.h>
#include <hf/controllers/KBInput.h>
#include "TestEntity.h"
class GameManager : public hf::AppManager
{

public:
	GameManager(int argc, char** argv);
	bool OnInit();
private:
	hf::KBInput m_kbInput;

	// Assets:
	hf::Map m_mTestMap1;
	hf::Map m_mTestMap2;

	hf::Script m_sHelloWorld;
	//Entities
	hf::TestEntity m_testEntity;

	hf::Image m_testImage1;
	hf::Image m_testEntityImage;
};

#endif // GAMEMANAGER_H
