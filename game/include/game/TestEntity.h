#ifndef TEST_ENTITY_H
#define TEST_ENTITY_H

#include <hf/Entity.h>
#include <hf/controllers/KBInput.h>
namespace hf{

class TestEntity : public Entity
{
	
public:
	TestEntity(iRect position) : 
		Entity("entityTestEntity", "testEntityImage", position, dVec3(2, 2, 0)),
		m_maxSpeed(500, 500, 1)
	{
	
	};

private:
	KBInput m_kBInput;
	dVec3 m_maxSpeed;
};
}

#endif // TEST_ENTITY_H
