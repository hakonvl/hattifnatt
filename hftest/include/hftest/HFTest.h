#ifndef HFTEST_H
#define HFTEST_H
#include <string>
#include <map>
#include <functional>
namespace hft
{
	class HFTest
	{
	public:
		using TestMethod = std::function<int()>;

		void AddTest(std::string name, TestMethod method);

		void ExecuteTests();

		void ExecuteTest(const std::string& name);
	private:
		struct Test
		{
			Test(){};
			Test(std::string name, TestMethod method) : name(name), testMethod(method){};
			const std::string name = "";
			TestMethod testMethod;
			int returnValue = 0;
			bool executed = false;

		};
		std::map<std::string, Test> m_tests;
	};
}


#endif