#include "HFtest.h"
#include <iostream>
namespace hft
{
	void HFTest::AddTest(std::string name, TestMethod method)
	{
		m_tests.emplace(name, Test{ name, method });
	}

	void HFTest::ExecuteTest(const std::string& name)
	{
		std::cout << "Executing tests..." << std::endl << "1/1 " << m_tests[name].name;
		m_tests[name].returnValue = m_tests[name].testMethod();
		if (m_tests[name].returnValue == 0)
			std::cout << "[OK]" << std::endl;
		else
			std::cout << "[FAILED]" << std::endl;
	}

	void HFTest::ExecuteTests()
	{
		std::cout << "Executing tests..." << std::endl;
		int i = 1;
		int no = m_tests.size();
		for (auto& it : m_tests)
		{
			std::cout << i << "/" << no << " " << it.second.name;
			it.second.returnValue = it.second.testMethod();
			++i;
			if (it.second.returnValue == 0)
				std::cout << "[OK]" << std::endl;
			else
				std::cout << "[FAILED]" << std::endl;
		}
	}
}