Hattifnatt engine (and soon-to-be editor)
==========

Compiles and runs on MS Windows and Linux. Developed and tested with Visual Studio 2014 on Windows, and KDevelop/g++ on Linux.

Building
--------
1. Open Cmake on CMakeLists.txt in the root folder of the repo. Select correct paths for all libraries that CMake does  not find by it self. Hattifnatt depends on Lua, LuaBind, SDL2 and SDL2_image. On Linux, just install them via the package manager, and CMake should find them. On Windows download [this](http://hakonvl.net/files/hattifnatt-win-deps.zip) file. (Located on a slow connection-server, please be patient, and please don't kill my network connection), and place it in a suitable location (For example c:/). 
2. Genereate build files for wanted IDE or build system, and put them in a usable folder (debug/ and release/ for most build systems, or build/ for multiple configuration build systems like Visual Studio)
3. When generating the project files, the following projects will be generated:
  * Game - Game spesific code, compiled as lib. To be replaced by the various  games that use the engine
  * hflauncher - Launcher executable. Links against hattifnatt
  * Hattifnatt - the engine, compiled as lib. Links staticily (TODO: Link dynamicly)
  * hftest - Soon-to-be unit test driver for the engine. Compiles as static lib
4. All binaries and libraries are currently placed in bin/. On some systems (For example Visual Studio), you have to manually set the working directory to the directory where the binaries are placed. (For multiple-configuration-generators the build system now copies all assets to all default build targets respective folders. We should conider just copying them to bin/, and set that as the working directory for all build targets)
 
Documentation
-------------
Most code should be documented with Doxygen syntax. Run doxygen on supplied Doxyfile to generate docs. Some work is needed to update and make sure all code is documented.

##Lua##
C++ classes, methods and  variables exposed to Lua. Classes and methods are in the hf namespace.
Format:
Lua name(C++name)
### Entity ###
 - name(GetName())
 - position(GetPosition(), SetPosition())
 - speed (GetSpeed(), SetSpeed())
 - accel (GetAccel(), SetAccel())
 - imageName (GetImageName(), SetImageName())
 Exposed in processes/EntityManager.cpp
 
### EntityManager ###
 - AddEntity() (AddEntity())
 - AddSkeleton() (AddSkeleton())
 
 Exposed as entityManager
 Exposed in processes/EntityManager.cpp

### iVec3, uiVec3, dVec3 and fVec3 ###
 - + (+)
 - - (-)
 - * (*)
 - / (/)
 - Lenght (Length())
 
  Exposed in processes/LuaBind.cpp
  
### iTetra, uiTetra, fTetra, dTetra ###
 - a (GetA)
 - b (GetB)
 - c (GetC)
 - d (GetD)
 - left (GetLeft)
 - right (GetRight)
 - top (GetTop)
 - bottom (GetBottom)
 - + (+)
 - - (-)
 
 Exposed in processes/LuaBind.cpp
 
### iRect, uiRect, fTetra, dTetra
 Derives from belonging Tetragon instance
 Exposed in processes/LuaBind.cpp


